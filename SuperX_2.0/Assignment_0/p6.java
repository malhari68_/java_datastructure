// 6) WAP to find number in which has numbers on its left is less than or equal to it self.

class LeftLessEq{

	public static void main(String[]args){

		int num = 456975962;
		int temp = num;
		int count = 0;
		while(num != 0){
			
			count++;
			num = num/10;
		}
		int arr[] = new int[count];
		int t = count;
		while(temp != 0){
			
			int rem = temp%10;
			arr[count-1] = rem;
			temp = temp/10;
			count--;
		}
		int max = Integer.MIN_VALUE;
		for(int i = 0;i < t;i++){
			
			if(arr[i] > max)
			max = arr[i];
		}
			
		System.out.println(max);		
	}
}


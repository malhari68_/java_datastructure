// 8) WAP to find occurances of vowels in given String.

class DemoVowels{
	
	public static void main(String[]args){
		String str = "adghtioseobi";
		char arr[] = str.toCharArray();
		int arr2[] = new int[5];
		for(int i = 0;i < str.length();i++){

			if(arr[i] == 'a')
				arr2[0]++;
			
			if(arr[i] == 'e')
				arr2[1]++;
			
			if(arr[i] == 'i')
				arr2[2]++;
			
			if(arr[i] == 'o')
				arr2[3]++;
			
			if(arr[i] == 'u')
				arr2[4]++;
		}	

		if(arr2[0] > 0)
			System.out.println("a = " + arr2[0]);
		
		if(arr2[1] > 0)
			System.out.println("e = " + arr2[1]);
		
		if(arr2[2] > 0)
			System.out.println("i = " + arr2[2]);
		
		if(arr2[3] > 0)
			System.out.println("o = " + arr2[3]);
		
		if(arr2[4] > 0)
			System.out.println("u = " + arr2[4]);
	}
}

// 3) WAP to print the pattern
//    D C B A
//    e f g h
//    F E D C
//    g h i j

class Pattern1{

	public static void main(String[]args){
		

		for(int i = 1;i <= 4;i++){
			int ch = 67 + i;	
			for(int j = 1;j <= 4;j++){

				if(i%2 == 0){
					
					System.out.print((char)(ch+32)+" ");
					ch++;
				}else{
					
					System.out.print((char)ch+" ");
					ch--;
				}
			}
			System.out.println();
		}
	}
}


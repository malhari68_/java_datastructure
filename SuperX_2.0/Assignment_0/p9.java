// 9) WAP to take string from user & convert all even indexes of string to uppercase & odd indexes of string to lowercase.

import java.io.*;
class DemoUppLow{

	public static void main(String[]args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str=br.readLine();
		char arr[] = str.toCharArray();
		for(int i = 0;i < arr.length;i++){

			if(i%2 == 0){
				
				if((int)arr[i] > 96)
				arr[i] = (char)((int)arr[i] - 32);
			}else{
				
				if((int)arr[i] < 96)
					
					arr[i] = (char)((int)arr[i] + 32);
			}
		}
		for(int i = 0;i < arr.length;i++){

			System.out.print(arr[i]+" ");
		}
	}
}


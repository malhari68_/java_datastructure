// 1) WAP to print the factorial of even numbers in given number.

class FactEvenNo{

	public static void main(String[]args){

		int num = 256946;
		
		int rev = 0;
		while(num!=0){
			int rem = num%10;
			rev = (rev*10) + rem;
			num = num/10;
		}
		
		System.out.println(rev);
		
		while(rev!=0){
			int rem = rev%10;
			if(rem%2 == 0){
				int fact = 1;
				for(int i = 1;i <= rem;i++){
					
					fact = fact*i;
				}
				System.out.println(fact);
			}
			rev = rev/10;
		}
	}
}


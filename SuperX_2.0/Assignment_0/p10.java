// 10) WAP to check whether given string is palindrome or not.

import java.io.*;
class DemoPalindrome{

	public static void main(String[]args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = br.readLine();
		char arr[] = str.toCharArray();
		int count = arr.length;
		char arr2[] = new char[count];
		
		for(int i = 0;i < arr.length;i++){
				
			arr2[i]=arr[--count];
		}
		int i = 0;
		int flag = 0;
		while(i < arr.length){
			if((int)arr[i] != (int)arr2[i]){
			 	
				System.out.println("not palindrome");
				flag = 1;
				break;
			}
			
			i++;
		}
		
		if(flag == 0)
			System.out.println("palindrome");
	}
}


// 4) WAP to print the pattern
// 	A b C d E
// 	e D c B
// 	B c D
// 	d C
// 	C
import java.util.*;

class Pattern2{

	public static void main(String[]args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter rows & column count");
		int c = sc.nextInt();
		
		int ch = 65;

		for(int i = 1;i <= c;i++){
				
			for(int j = i;j <= c;j++){
				
				if(i%2 != 0){
					if(j%2 == 0){
						
						System.out.print((char)(ch+32));
						ch++;
					}else{
						
						System.out.print((char)ch);
						ch++;
					}
				}else{
					if(j%2==0){
						
						System.out.print((char)(ch+31));
						ch--;
					}else{
						
						System.out.print((char)(ch-1));
						ch--;
					}					
				}
			}
			System.out.println();
		}
	}
}


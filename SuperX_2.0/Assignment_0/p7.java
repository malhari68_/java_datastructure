// 7) Take the size of array from user create two arrays of that size .
//Initialize all second array elements as zero(0).
//For the first array take all elements from user check if the elements in 1st array are  even or not.
//If it's even then replace the value of 2nd array of that index with 1 & print both array.
import java.util.*;

class DemoEven{
	
	public static void main(String[]args){
		
		Scanner sc = new Scanner(System.in);
		System.out.println("enter size of array");
		int n = sc.nextInt();
		int arr1[] = new int[n];
		System.out.println("enter elements of array");
		int arr2[] = new int[n];
		for(int i = 0;i < n;i++){
			
			arr1[i] = sc.nextInt();
		}
		for(int i = 0;i < n;i++){
			
			if(arr1[i]%2 == 0){
				
				arr2[i]=1;
			}else{
				
				arr2[i]=0;
			}
		}
		for(int i = 0;i < n;i++){
			
			System.out.print(arr1[i]+" ");
		}
		System.out.println();
		for(int i = 0;i < n;i++){
			
			System.out.print(arr2[i]+" ");
		}
		
		System.out.println();
	}
}



		

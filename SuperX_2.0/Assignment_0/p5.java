// 5) WAP to reverse number & put successive number sum into array & print it..

class RevSuccNo{

	public static void main(String[]args){

		int num = 45689;
		int rev = 0;
		int count = 0;
		while(num != 0){
			
			int rem = num%10;
			rev = (rev*10) + rem;
			num = num/10;
			count++;
		}
		int arr[] = new int[count];
		for(int i = count-1;i >= 0;i--){
			
			int rem = rev%10;
			arr[i] = rem;
			rev = rev/10;
		}
		for(int j = 0;j < count-1;j++){
			
			arr[j] = arr[j] + arr[j+1];
		}
		for(int j = 0;j < count;j++){
			
			System.out.print(arr[j]+" ");
		}

		System.out.println();
	}
}


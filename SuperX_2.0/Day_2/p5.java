import java.util.*;
class Demo{

	public static void main(String[]args){
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter String : ");
		String str = sc.next();
	        str = str.toLowerCase();
	        char c[] = str.toCharArray();
		int count = 0;
		for(int i = 0;i < c.length;i++){
		
			if(c[i] == 'a' || c[i] == 'e' || c[i] == 'i' || c[i] == 'o' || c[i] == 'u'){
			
				count++;
			}
		}

		System.out.println("String contains vowels is : " + count);
	}
}

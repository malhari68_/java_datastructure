import java.util.*;
class Demo{

	public static void main(String[]args){
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Str : ");
		String str = sc.next();
		char c[] = str.toCharArray();
		int s = c.length;
		int count = 0;
		for(int i = 0;i < c.length;i++){
		
			if(Character.isAlphabetic(c[i])){
			
				count++;
			}
		}

		if(count == s){
		
			System.out.println("String contains letters");
		}else{
		
			System.out.println("String contains characters");
		}
	}
}

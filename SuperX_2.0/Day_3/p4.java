class Demo{

	public static void main(String[]args){
	
		int start = 25435;
		int end = 25449;
		for(int i = start;i <= end;i++){
		
			int N = i;
			int res = 0;
			while(N > 0){
			
				int s = N % 10;
				res = (res * 10) + s;
				N = N / 10;
			}

			System.out.println(res);
		}
	}
}

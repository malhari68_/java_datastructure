class Demo{

	public static void main(String[]args){
	
		int N = 4;
		char c = 'A';
		for(int i = 1;i <= N;i++){
		
			for(int j = 1;j <= N;j++){
			
				if(i%2 != 0){
				
					System.out.print(c + " ");
					c++;
				}else{
				
					c--;
					System.out.print(c + " ");
				}
			}

			System.out.println();

		}
	}
}

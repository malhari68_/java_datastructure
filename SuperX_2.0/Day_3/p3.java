import java.util.*;
class Demo{

	public static void main(String[]args){
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter num : ");
		int num = sc.nextInt();
		int s = 0;
		int temp = num;
		while(num > 0){
		
		      int res = num % 10;
		      s = (s * 10) + res;
		      num = num / 10;
		}

		if(s == temp){
		
			System.out.println(temp + " is a palindrome num");
		}else{
		
			System.out.println(temp + " is not palindrome num");
		}
	}
}


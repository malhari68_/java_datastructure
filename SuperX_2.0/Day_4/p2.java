class Demo{

	public static void main(String[]args){
	
		int N = 4;
		char x = 'A';
		for(int i = 1;i <= N;i++){
		
			char c = x;
			for(int j = 1;j <= i;j++){
			
				System.out.print(c + " ");
				c--;
			}

			x++;
			System.out.println();
		}
	}
}

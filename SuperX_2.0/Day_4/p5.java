import java.util.*;
class Demo{

	public static void main(String[]args){
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter string : ");
		String str = sc.next();
		char c[] = str.toCharArray();
		char res[] = new char[c.length];
		for(int i = 0;i < c.length;i++){
		
			if(Character.isUpperCase(c[i])){
			
				res[i] = Character.toLowerCase(c[i]);
			}else{
			
				res[i] = Character.toUpperCase(c[i]);
			}
		}

		/*for(int i = 0;i < res.length;i++){
		
			System.out.print(res[i] + " ");
		}

		System.out.println();*/

		String string = new String(res);
		System.out.println(string);
	}
}

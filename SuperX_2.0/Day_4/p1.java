class Demo{

	public static void main(String[]args){
	
		int N = 5;
		for(int i = 1;i <= N;i++){
		
			char c = 'A';
			for(int j = 1;j <= N - 1;j++){
			
				if(i%2 != 0){
				
					System.out.print(c + " ");
					c++;
				}else{
				
					System.out.print("# ");
				}
			}

			System.out.println();
		}
	}
}

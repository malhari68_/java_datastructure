class Demo{

	public static void main(String[]args){
	
		int N = 4;
		int num1 = 1;
		int num2 = 2;
		for(int i = 1;i <= N;i++){
		
			for(int j = 1;j <= N;j++){
			
				if(i%2 != 0){
				
					System.out.print(num1 + "  ");
				        num1 = num1 + 2;
				}else{
				
					System.out.print(num2 + "  ");
					num2 = num2 + 2;
				}
			}

			System.out.println();
		}
	}
}

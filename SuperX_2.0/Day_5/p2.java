class Demo{

	public static void main(String[]args){

		int N = 4;
		for(int i = 1;i <= N;i++){
		
			char c1 = 'a';
			char c2 = 'A';
			for(int j = 1;j <= i;j++){
			
				if(i%2 != 0){
				
					System.out.print(c1 + " ");
					c1++;
				}else{
				
					System.out.print(c2 + " ");
					c2++;
				}
			}

			System.out.println();
		}
	}
}

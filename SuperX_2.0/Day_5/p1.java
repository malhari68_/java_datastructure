class Demo{

	public static void main(String[]args){
	
		int N1 = 1;
		int N2 = 10;

		for(int i = N1;i <= N2;i++){
		
			int fact = 1;
			for(int j = 1;j <= i;j++){
			
				fact = fact * j;
			}

			System.out.println(fact);
		}
	}
}

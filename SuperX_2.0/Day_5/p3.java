import java.util.*;
class Demo{

	public static void main(String[]args){
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Num : ");
		int N = sc.nextInt();
		int temp = N;
		int sum = 0;
		while(N > 0){
		
			int fact = 1;
			int res = N % 10;
			for(int i = 1;i <= res;i++){
			
				fact = fact * i;
			}

			sum = sum + fact;
			N = N / 10;
		}

		if(sum == temp){
		
			System.out.println(temp + " is a strong num");
		}else{
		
			System.out.println(temp + " is not a strong num");
		}
	}
}

// 1) Pattern
//
//           1 2 3 4
//           4 5 6 7
//           6 7 8 9
//           7 8 9 10

class Demo{

	public static void main(String[]args){
	
		int x = 1;
		int N = 4;
		for(int i = 1;i <= N;i++){
		
			for(int j = 1;j <= N;j++){
			
				System.out.print(x + " ");
				x++;
			}

			x = x - i;
			System.out.println();
		} 
	}
}

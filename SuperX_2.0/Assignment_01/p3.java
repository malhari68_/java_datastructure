// 3) Pattern
//
//           5
//           6 8
//           7 10 13
//           8 12 16 20
//           9 14 19 24 29

class Demo{

	public static void main(String[]args){
	
		int x = 5;
		int N = 5;
		for(int i = 1;i <= N;i++){
		
			int temp = x;
			for(int j = 1; j <= i;j++){
			
				System.out.print(temp + " ");
				temp = temp + i;
			}
		        x++;	
			System.out.println();
		}
	}
}

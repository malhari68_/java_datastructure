// 2) Pattern
//
//          0
//          3 8
//          15 24 35
//          48 63 80 99

class Demo{

	public static void main(String[]args){	

		int x = 1;
		int N = 4;
		for(int i = 1;i <= N;i++){
		
			for(int j = 1;j <= i;j++){
			
				System.out.print(((x * x) - 1) + " ");
				x++;
			}

			System.out.println();
		}
	}
}

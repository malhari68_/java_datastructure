enum ProLang{

	C,CPP,Java,Python
}
class EnumDemo{

	public static void main(String[]args){
	
		System.out.println(ProLang.C);   //C
		System.out.println(ProLang.CPP); //CPP
		System.out.println(ProLang.Java); //Java
		System.out.println(ProLang.Python); //Python
	}
}

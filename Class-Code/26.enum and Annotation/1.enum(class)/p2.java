enum ProLang{

	C,CPP,Java,Python
}
class EnumDemo{

	public static void main(String[]args){
	
		ProLang lang = ProLang.Java;
		System.out.println(lang.ordinal());  //index = 2
		System.out.println(lang.toString()); //Java
		System.out.println(lang);            //Java
		System.out.println(lang.name());     //Java
	}
}

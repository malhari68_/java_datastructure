class Parent{

	@Deprecated
	void m1(){
	
		System.out.println("Parent-m1");
	}
}
class Client{

	public static void main(String[]args){
	
		Parent p = new Parent();
		p.m1();
	}
}
/*Note: p1.java uses or overrides a deprecated API.
 * Note: Recompile with -Xlint:deprecation for details.*/

//OP : Parent-m1

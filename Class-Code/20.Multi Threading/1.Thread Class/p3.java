class MyThread extends Thread{

	public void run(){
	
		System.out.println("In MyThread run");
		System.out.println("MyThread : " + Thread.currentThread().getName());
	}
	public void start(){
	
		System.out.println("In MyThread start");
	        run();	
	}
}
class ThreadDemo{

	public static void main(String[]args){
	
		MyThread obj = new MyThread();
		obj.start();
		System.out.println("ThreadDemo : " + Thread.currentThread().getName());
	}
}

class MyThread extends Thread{

        MyThread(String str){

                super(str);
        }

	MyThread(){
	
		super();
	}

        public void run(){

                System.out.println("MyThread thread_name :" + getName());
                System.out.println("MyThread group_name : " + Thread.currentThread().getThreadGroup());
        }
}
class ThreadGroupDemo{

        public static void main(String[]args){

                MyThread obj1 = new MyThread("xyz");
                obj1.start();
                
		MyThread obj2 = new MyThread("pqr");
                obj2.start();
                
		MyThread obj3 = new MyThread();
                obj3.start();
        }
}

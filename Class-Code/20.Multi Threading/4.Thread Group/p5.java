class MyThread extends Thread{

	MyThread(ThreadGroup tg,String str){
	
		super(tg,str);
	}

	public void run(){
	
		System.out.println("MyThread : " + Thread.currentThread());
	       try{
	       
		       Thread.sleep(2000);
	       }catch(InterruptedException ie){
	       
		       System.out.println(ie.toString());
	       }	
	}
}
class ThreadGroupDemo{

	public static void main(String[]args)throws InterruptedException{
	
		ThreadGroup pThreadGP = new ThreadGroup("India");

		MyThread obj1 = new MyThread(pThreadGP,"Maharastra");
		MyThread obj2 = new MyThread(pThreadGP,"Goa");
		obj1.start();
		obj2.start();

		ThreadGroup cThreadGP1 = new ThreadGroup(pThreadGP,"Pakistan");

		MyThread obj3 = new MyThread(cThreadGP1,"karachi");
		MyThread obj4 = new MyThread(cThreadGP1,"Lahore");
		obj3.start();
		obj4.start();

		ThreadGroup cThreadGP2 = new ThreadGroup(pThreadGP,"Bangladesh");

		MyThread obj5 = new MyThread(cThreadGP2,"Dhaka");
		MyThread obj6 = new MyThread(cThreadGP2,"Mirpur");
		obj5.start();
		obj6.start();

		System.out.println("Active Count : " + pThreadGP.activeCount());
		System.out.println("Active Group Count : " + pThreadGP.activeGroupCount());
	}
}

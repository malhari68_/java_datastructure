class MyThread implements Runnable{

        public void run(){

                System.out.println("MyThread : " + Thread.currentThread());
               try{

                       Thread.sleep(2000);
               }catch(InterruptedException ie){

                       System.out.println(ie.toString());
               }
        }
}
class ThreadGroupDemo{

        public static void main(String[]args)throws InterruptedException{

                ThreadGroup pThreadGP = new ThreadGroup("India");

                MyThread obj1 = new MyThread();
		Thread t1 = new Thread(pThreadGP,obj1,"Maharastra");
                MyThread obj2 = new MyThread();
		Thread t2 = new Thread(pThreadGP,obj2,"Goa");
                t1.start();
                t2.start();

                ThreadGroup cThreadGP1 = new ThreadGroup(pThreadGP,"Pakistan");

                MyThread obj3 = new MyThread();
		Thread t3 = new Thread(cThreadGP1,obj3,"Karachi");
                MyThread obj4 = new MyThread();
		Thread t4 = new Thread(cThreadGP1,obj4,"Lahore");
                t3.start();
                t4.start();

                ThreadGroup cThreadGP2 = new ThreadGroup(pThreadGP,"Bangladesh");

                MyThread obj5 = new MyThread();
		Thread t5 = new Thread(cThreadGP2,obj5,"Dhaka");
                MyThread obj6 = new MyThread();
		Thread t6 = new Thread(cThreadGP2,obj6,"Mirpur");
                t5.start();
                t6.start();

                System.out.println("Active Count : " + pThreadGP.activeCount());
                System.out.println("Active Group Count : " + pThreadGP.activeGroupCount());
        }
}

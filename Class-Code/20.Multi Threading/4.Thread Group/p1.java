class MyThread extends Thread{

	MyThread(String str){
	
		super(str);
	}

	public void run(){
	
		System.out.println("MyThread thread_name :" + getName());
		System.out.println("MyThread group_name : " + Thread.currentThread().getThreadGroup()); 
	}
}
class ThreadGroupDemo{

	public static void main(String[]args){
	
		MyThread obj = new MyThread("xyz");
		obj.start();
	}
}

class MyThread extends Thread{

	MyThread(ThreadGroup tg,String str){
	
		super(tg,str);
	}
	
	public void run(){
	
		System.out.println("MyThread : " + Thread.currentThread());
	}
}
class ThreadGroupDemo{

	public static void main(String[]args){
	
		ThreadGroup pThreadGp = new ThreadGroup("Core2Web");

		MyThread obj1 = new MyThread(pThreadGp,"C");
		MyThread obj2 = new MyThread(pThreadGp,"Java");
		MyThread obj3 = new MyThread(pThreadGp,"Python");

		obj1.start();
		obj2.start();
		obj3.start();
		
		ThreadGroup cThreadGp = new ThreadGroup(pThreadGp,"Incubator");

		MyThread obj4 = new MyThread(cThreadGp,"Flutter");
		MyThread obj5 = new MyThread(cThreadGp,"React.Js");
		MyThread obj6 = new MyThread(cThreadGp,"SpringBoot");
		
		obj4.start();
		obj5.start();
		obj6.start();
	}
}

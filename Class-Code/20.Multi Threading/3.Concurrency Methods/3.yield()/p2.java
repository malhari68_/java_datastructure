class MyThread extends Thread{

        public void run(){

                System.out.println("MyThread name : " + Thread.currentThread().getName());
                for(int i = 0;i < 11;i++){

                        System.out.println("Thread-0");
                }
        }
}
class ThreadYieldDemo{

        public static void main(String[]args){

                MyThread obj = new MyThread();
                obj.start();

                obj.yield();

                System.out.println("ThreadYieldDemo name:" + Thread.currentThread().getName());
                for(int i = 0;i <11;i++){

                        System.out.println("In main");
                }

        }
}

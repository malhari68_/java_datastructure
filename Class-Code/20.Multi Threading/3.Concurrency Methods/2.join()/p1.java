class MyThread implements Runnable{

	public void run(){
	
		for(int i = 0;i < 10;i++){
		
			System.out.println("MyThread : " + Thread.currentThread());
		}
	}
}
class ThreadDemo{

	public static void main(String[]args)throws InterruptedException{
	
		MyThread obj = new MyThread();
		Thread t = new Thread(obj);
		t.start();
		
		t.join();

		for(int i = 0;i < 10;i++){
		
			System.out.println("ThreadDemo : " + t.currentThread());
		}
	}
}

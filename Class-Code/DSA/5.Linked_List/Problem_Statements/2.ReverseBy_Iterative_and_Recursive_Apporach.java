import java.util.*;
class Node{

	int data;
	Node next = null;
	
	Node(int data){
	
		this.data = data;
	}
}
class LinkedList{

	Node head = null;

	//addNode
	void addNode(int data){
	
		Node newNode = new Node(data);

		if(head == null){
		
			head = newNode;
		}else{
		
			Node temp = head;

			while(temp.next != null){
			
				temp = temp.next;
			}

			temp.next = newNode;
		}
	}

	//reverseItr
	void reverseItr(){	

		if(head == null || head.next == null)
			return;
		
		Node prev = null;
		Node curr = head;
		Node forward = null;

		while(curr != null){
		
			forward = curr.next;
			curr.next = prev;
			prev = curr;
			curr = forward;
		}

		head = prev;
	}

	//reverseRec
	void reverseRec(Node prev,Node curr){
	
		if(curr == null){
		
			head = prev;
			return;
		}else{
		
			Node forward = curr.next;
			curr.next = prev;
			prev = curr;
			curr = forward;
		}

		reverseRec(prev,curr);
	}

	//printDLL
	void printDLL(){
	
		if(head == null){
		
			System.out.println("LinkedList Empty");
		}else{
		
			Node temp = head;
			while(temp.next != null){
			
				System.out.print(temp.data + "->");
				temp = temp.next;
			}

			System.out.println(temp.data);
		}
	}
}
class Client{

	public static void main(String[]args){
	
		LinkedList ll = new LinkedList();

		Scanner sc = new Scanner(System.in);
		char ch;

		do{
		
			System.out.println("1.addNode");
			System.out.println("2.printDLl");
			System.out.println("3.reverse_Iterative_LinkList");
			System.out.println("4.reverse_Recursion_LinkList");

			System.out.println("Enter your choice : ");
			int choice = sc.nextInt();

			switch(choice){
			
				case 1:
					{
					
						System.out.println("Enter Data");
						int data = sc.nextInt();
						ll.addNode(data);
					}
					break;

				case 2:
					ll.printDLL();
					break;

				case 3:
				        ll.reverseItr();
				        break;
				
				case 4:
					{
					
						Node prev = null;
						Node curr = ll.head;
						ll.reverseRec(prev,curr);
					}
					break;

				default:
				       System.out.println("Wrong Choice");
			               break;	       
			}

			System.out.println("Do you want to continue...");
			ch = sc.next().charAt(0);

		}while(ch == 'Y' || ch == 'y');
	}
}

class Node{

	int data;
	Node next = null;
	Node prev = null;

	Node(int data){
	
		this.data = data;
	}
}
class reverse_linkedList{

	Node head = null;

	//addFirst
	void addFirst(int data){
	
		Node newNode = new Node(data);

		if(head == null){
		
			head = newNode;
		}else{
		
			newNode.next = head;
			head.prev = newNode;
			head = newNode;
		}
	}

	//autual code
	void fun(){
	
		if(head == null){
		
			System.out.println("LinkedList empty");
		}else{
		
			Node temp1 = head;
			Node temp2 = head;
			while(temp2 != null){
			
				Node temp3 = temp1.data;
				temp1.data = temp2.data;
				temp2.data = temp3;
			}

			temp1 = temp1.next;
			temp2--;
		}
			Node temp = head;
			while(temp.next != null){
			
				System.out.print(temp.data + "->");
				temp = temp.next;
			}

			System.out.println(temp.data);
	}

	//countNode
	int countNode(){
	
		if(head == null){
		
			return 0;
		}else{
		
			int count = 0;
			Node temp = head;
			while(temp != null){
			
				count++;
				temp = temp.next;
			}

			return count;
		}
	}

	//printDLL
	void printDLL(){
	
		if(head == null){
		
			System.out.println("LinkedList Empty");
		}else{
		
			Node temp = head;
			while(temp.next != null){
			
				System.out.print(temp.data + "->");
				temp = temp.next;
			}

			System.out.println(temp.data);
		}
	}
}
class Client{

	public static void main(String[]args){
	
		reverse_linkedList ll = new reverse_linkedList();
		ll.addFirst(40);
		ll.addFirst(30);
		ll.addFirst(20);
		ll.addFirst(10);
		ll.printDLL();
		int count = ll.countNode();
		System.out.println("Count => " + count);
	}
}

class QuickSort{

	int partition(int arr[],int start,int end){
	
		int pivot = arr[end];
		int i = start - 1;
		for(int j = start;j < end;j++){
		
			if(arr[j] <= pivot){
			
				i++;
				int temp = arr[j];
				arr[j] = arr[i];
				arr[i] = temp;
			}
		}

		int temp = arr[i+1];
		arr[i+1] = arr[end];
		arr[end] = temp;

		return i + 1;
	}

	void quickSort(int arr[],int start,int end){
	
		if(start < end){
		
			int pivotIndex = partition(arr,start,end);
			quickSort(arr,start,pivotIndex - 1);
			quickSort(arr,pivotIndex + 1,end);
		}
	}

	public static void main(String[]args){
	
		int arr[] = new int[]{12,7,6,14,5,15,10};
		int start = 0;
		int end = arr.length - 1;
		QuickSort obj = new QuickSort();
		obj.quickSort(arr,start,end);

		for(int i = 0;i < arr.length;i++){
		
			System.out.print(arr[i] + " ");
		}

		System.out.println();
	}
}

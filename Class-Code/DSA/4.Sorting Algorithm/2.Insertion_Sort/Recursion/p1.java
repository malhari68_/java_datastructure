class InsertionSort{

	void insertionSort(int arr[],int i,int n){

		int element = arr[i];
		int j = i - 1;
		while(j >= 0 && arr[j] > element){
		
			arr[j+1] = arr[j];
			j--;
		} 

		arr[j+1] = element;

		if(i+1 <= n){		
			
			insertionSort(arr,i + 1,n);
		}
	}

	public static void main(String[]args){
	
		int arr[] = new int[]{8,3,1,7,5,4,2};
		InsertionSort obj = new InsertionSort();
		obj.insertionSort(arr,1,arr.length - 1);

		for(int i = 0;i < arr.length;i++){
		
			System.out.print(arr[i] +  " ");      //1 2 3 4 5 7 8
		}

		System.out.println();
	}
}

class SelectionSort{

	void selectionSort(int arr[],int i,int n){
	
                if(i == n)
			return;

		int minIndex = i;
		for(int j = i+1;j < n;j++){
		
			if(arr[minIndex] > arr[j]){
			
				minIndex = j;
			}
		}

		int temp = arr[i];
		arr[i] = arr[minIndex];
		arr[minIndex] = temp;

		selectionSort(arr,i+1,n);
	}

	public static void main(String[]args){
	
		int arr[] = new int[]{9,2,7,3,1,8,4,6};
		SelectionSort obj = new SelectionSort();
		obj.selectionSort(arr,0,arr.length);

		for(int i = 0;i < arr.length;i++){
		
			System.out.print(arr[i] + " ");     //1 2 3 4 6 7 8 9
		}

		System.out.println();
	}
}

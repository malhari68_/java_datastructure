import java.util.*;

class QueueDemo{

	public static void main(String[]args){
	
		Queue<Integer> q = new LinkedList<Integer>();        //LinkedList Cha parent => Deque ahe and 
						   //Deque ch parenet => Queue ahe
	        q.offer(10);
	        q.offer(20);
	        q.offer(30);         // push <=> offer <=> enqueue(standarad method)
		q.add(40);          

		System.out.println(q);

		System.out.println(q.peek());
		System.out.println(q.element());
		System.out.println(q);

		System.out.println(q.poll());        // pop <=> poll <=> dequeue(standard method)
		System.out.println(q);
		
		System.out.println(q.remove());
		System.out.println(q);
	}
}

import java.util.*;
class Node{

	int data;
	Node next;

	Node(int data){
	
		this.data = data;
	}
}
class CircularQueue{

	int maxSize;
	int front;
	int rear;
	Node head;

	CircularQueue(int size){
	
		this.maxSize = size;
		this.head = null;
		this.front = -1;
		this.rear = -1;
	}

	//enqueue()
	void enqueue(int data){
	
		Node newNode = new Node(data);
		
		if((front == 0 && rear == maxSize - 1) || ((rear + 1)%maxSize == front)){
		
			System.out.println("Queue is full");
			return;
		}else if(front == -1){
		
			front = rear = 0;
			head = newNode;

		}else if(rear == maxSize -1 && front != 0){
		
			rear = 0;
		}else{
		
			rear++;
			Node temp = head;
			while(temp.next != null){
			
				temp = temp.next;
			}


		}
	}
}
class Client{

	public static void main(String[]args){
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of queue :");
		int size = sc.nextInt();

		CircularQueue cq = new CircularQueue(size);

		char ch;
		do{
		
			System.out.println("1.enqueue()");
			System.out.println("2.dequeue()");
			System.out.println("3.empty()");
			System.out.println("4.front()");
			System.out.println("5.rear()");
			System.out.println("6.printQueue()");

			System.out.println("Enter your choice : ");
			int choice = sc.nextInt();

			switch(choice){
			
				case 1:
					{
					
						System.out.println("Enter data in circularQueue : ");
						int data = sc.nextInt();
						cq.enqueue(data);
					}
					break;

				default :
					System.out.println("Wrong choice");
					break;
			}

			System.out.println("Do you have continue...");
			ch = sc.next().charAt(0);

		}while(ch == 'Y' || ch == 'y');
	}
}

import java.util.*;

class Node{

	Node next;
	int data;

	Node(int data){
	
		this.data = data;
	}
}
class Queue{

	int maxSize;
	Node rear;
	Node head = null;
	int count = 0;

	Queue(int size){
	
		this.maxSize = size;
		this.rear = null;
	}

	//enqueue()
	void enqueue(int data){
	
		Node newNode = new Node(data);

		if(count < maxSize){
		
			if(head == null){
			
				head = newNode;
				rear = head;
			}else{
			
				Node temp = head;
				while(temp.next != null){
				
					temp = temp.next;
				}
				temp.next = newNode;
				rear = temp.next;
			}

			count++;
		}else{
		
			System.out.println("Queue is full");
		}
	}

	//dequeue
	int dequeue(){
	
		if(head == null){
		
			System.out.println("Queue is empty");
			return -1;
		}else if(count == 1){
		
			int ret = head.data;
			head = null;
			return ret;
		}else{
		
			int ret = head.data;
			head = head.next;
			if(head == null){
			
				rear = head;
				count = 0;
			}
			return ret;
		}
	}

	//empty()
	boolean empty(){
	
		if(head == null){
		
			return true;
		}else{
		
			return false;
		}
	}

	//front()
	int front(){
	
		if(head == null){
		
			System.out.println("Queue is empty");
			return -1;
		}else{
		
			return head.data;
		}
	}

	//rear()
	int rear(){
	
		if(head == null){
		
			System.out.println("Queue is empty");
			return -1;
		}else{
		
			return rear.data;
		}
	}

	//printQueue
	void printQueue(){
	
		if(head == null){
		
			System.out.println("Queue is empty");
		}else{
		
			Node temp = head;
			while(temp.next != null){
			
				System.out.print(temp.data + "=>");
				temp = temp.next;
			}

			System.out.println(temp.data);
		}
	}
}
class Client{

	public static void main(String[]args){
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of queue : ");
		int size = sc.nextInt();

		Queue q = new Queue(size);

		char ch;

		do{
		
			System.out.println("1.enqueue()");
			System.out.println("2.dequeue()");
			System.out.println("3.empty()");
			System.out.println("4.front()");
			System.out.println("5.rear()");
			System.out.println("6.printQueue()");

			System.out.println("Enter your choice : ");
			int choice = 0;
			try{
				choice = sc.nextInt();

			}catch(InputMismatchException obj){
			
				System.out.print("Please enter integer data : ");
			}
			sc.nextLine();

			System.out.println(choice);

			switch(choice){
			
				case 1:
					{
					
						System.out.println("Enter data in queue : ");
						int data = sc.nextInt();
						q.enqueue(data);
					}
					break;

				case 2:
					{
					
						int ret = q.dequeue();
						if(ret != -1){
						
							System.out.println(ret + " => popped");						}
					}
					break;

				case 3:
					{
					
						boolean ret = q.empty();
						if(ret){
						
							System.out.println("Queue is empty");
						}else{
						
							System.out.println("Queue is not empty");
						}
					}
					break;

				case 4:
					{
					
						int ret = q.front();
						if(ret != -1){
						
							System.out.println(ret);
						}
					}
					break;

				case 5:
					{
					
						int ret = q.rear();
						if(ret != -1){
						
							System.out.println(ret);
						}
					}
					break;
				
				case 6:
					{
					
						q.printQueue();
					}
					break;

				default :
					System.out.println("Wrong choice");
					break;
			}

			System.out.println("Do you have continue....");	
			ch = sc.next().charAt(0);

		}while(ch == 'Y' || ch == 'y');
	}
}

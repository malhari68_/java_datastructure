// maximum subarray sum :
// given an integer array of size N.
// find the contiguous subarray(containing atleast one number) which has the largest sum and return its sum.
// I/P : [-2,1,-3,4,-1,2,1,-5,4]
// O/P : 6
// Explanation : [4,-1,2,1] has the largest sum = 6
//

class largestSumSubarray{

	public static void main(String[]args){
	
		int arr[] = new int[]{-2,1,-3,4,-1,2,1,-5,4};
		int prefixsum[] = new int[arr.length];
		int maxsum = Integer.MIN_VALUE;
		prefixsum[0] = arr[0];
		for(int i = 1;i < arr.length;i++){
		
			prefixsum[i] = prefixsum[i - 1] + arr[i];
		}

		for(int i = 0;i < arr.length;i++){
		
			for(int j = i;j < arr.length;j++){
			
				int sum = 0;

				if(i == 0)
					sum = prefixsum[j];

				else
					sum = prefixsum[j] - prefixsum[j - 1];

				if(sum > maxsum)
					maxsum = sum;
			}
		}

		System.out.println(maxsum);
	}
} 

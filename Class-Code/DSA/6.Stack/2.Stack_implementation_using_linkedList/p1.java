import java.util.*;
class Node{

	int data;
	Node next = null;

	Node(int data){
	
		this.data = data;
	}
}
class LinkedList{

	Node head = null;
	int size;
	int top = 0;

	LinkedList(int size){
	
		this.size = size;
	}

	//push
	void push(int data){
	
		if(top < size){
		
		        Node newNode = new Node(data);
		       	Node temp = head;

			if(head == null){
			
				head = newNode;
				top++;
			}else{
			
				while(temp.next != null){
				
					temp = temp.next;
				}

				temp.next = newNode;
				top++;
			}
		}else{
		
			System.out.println("Stack is full");
		}
	}

	//pop()
	void pop(){
	
		if(head == null){
		
			System.out.println("Stack is empty");
			return;
		}

		if(top == 1){

			head = null;
		}else{
		
			Node temp = head;
			while(temp.next.next != null){
			
				temp = temp.next;
			}

			temp.next = null;
			top--;
		}
	}

	//peek()
	int peek(){
	
		if(head == null){
		
			System.out.println("Stack is empty");
			return 0;
		}else{
		
			Node temp = head;
			while(temp.next != null){
			
				temp = temp.next;
			}

			return temp.data;
		}
	}

	//empty
	boolean empty(){
	
		if(head == null){
		
			return true;
		}else{
		
			return false;
		}
	}

	int stackSize(){
	
		return top;
	}

	//printStack
	void printStack(){
	
		if(head == null){
		
			System.out.println("LinkedList is empty");
		}else{
		
			Node temp = head;
			while(temp.next != null){
			
				System.out.print(temp.data + "->");
				temp = temp.next;
			}

			System.out.println(temp.data);
		}	
	}
}            
class Client{

	public static void main(String[]args){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size of Stack : ");
		int size = sc.nextInt();
		LinkedList ll = new LinkedList(size);
		
		char ch;

		do{
		
			System.out.println("1.push()");
			System.out.println("2.pop()");
			System.out.println("3.peek()");
			System.out.println("4.empty()");
			System.out.println("5.size()");
			System.out.println("6.printStack()");

			System.out.println("Enter your choice : ");
			int choice = sc.nextInt();

			switch(choice){
			
				case 1 :
					{
					
						System.out.println("Enter Element in stack : ");
						int data = sc.nextInt();
						ll.push(data);
					}
					break;

				case 2:
					{
					
						ll.pop();
					}
					break;

				case 3:
					{
					
						int value = ll.peek();
						System.out.println(value);
					}
					break;

				case 4:
					{
					
						System.out.println(ll.empty());
					}
					break;

				case 5:
					{
					
						int val = ll.stackSize();
						System.out.println("size of stack : " + val);
					}
					break;

				case 6:
					{
					
						ll.printStack();
					}
					break;

				default :
					System.out.println("Wrong choice");
			}

			System.out.println("Do you have continue....");
			ch = sc.next().charAt(0);

		}while(ch == 'Y' || ch == 'y');
	}
}

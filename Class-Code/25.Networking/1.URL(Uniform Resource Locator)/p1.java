import java.net.*;
import java.io.*;
class URLDemo{

	public static void main(String[]args)throws IOException{
	
		URL obj = new URL("https://www.core2web.in");
		System.out.println(obj + " : Port No : " + obj.getPort());
		System.out.println("____________________________________________________________________________");
		System.out.println(obj + " : Protocol : " + obj.getProtocol());
		System.out.println("____________________________________________________________________________");
		System.out.println(obj + " : File Name : " + obj.getFile());
		System.out.println("____________________________________________________________________________");
		System.out.println(obj + " :  : " + obj.getDefaultPort());
		System.out.println("____________________________________________________________________________");
		System.out.println(obj + " :  : " + obj.getHost());
		System.out.println("____________________________________________________________________________");
		System.out.println(obj + " :  : " + obj.getRef());
		System.out.println("____________________________________________________________________________");
	}
}
/*https://www.core2web.in : Port No : -1
 * ____________________________________________________________________________
 * https://www.core2web.in : Protocol : https
 * ____________________________________________________________________________
 * https://www.core2web.in : File Name :
 * ____________________________________________________________________________
 * https://www.core2web.in :  : 443
 * ____________________________________________________________________________
 * https://www.core2web.in :  : www.core2web.in
 * ____________________________________________________________________________
 * https://www.core2web.in :  : null
 * ____________________________________________________________________________*/

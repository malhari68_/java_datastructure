import java.net.*;
import java.io.*;
class IPAddress{

	public static void main(String[]args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter URL : ");
		String site = br.readLine();
		InetAddress ip = InetAddress.getByName(site);
		System.out.println( site +" : "+ "Ip Address : " + ip); 
	}
}
/*Enter URL :
 * www.youtube.com
 * www.youtube.comIp Address : www.youtube.com/142.250.199.174*/

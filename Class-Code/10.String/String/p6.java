class StringDemo{

	public static void main(String[]args){
	
		String str1 = "Kanha";
		String str2 = str1;
		String str3 = new String(str2);

		System.out.println("Str1 = " + System.identityHashCode(str1));
		System.out.println("Str2 = " + System.identityHashCode(str2));
		System.out.println("Str3 = " + System.identityHashCode(str3));
	}
}

class CharAtDemo{

	public static void main(String[]args){
	
		String str = "Core2Web";

		System.out.println(str.charAt(4));
		System.out.println(str.charAt(6));
		System.out.println(str.CharAt(8));
	}
}

//OP :
//CharAt.java:9: error: cannot find symbol
//                System.out.println(str.CharAt(8));
//                                     ^
//  symbol:   method CharAt(int)
//  location: variable str of type String
//1 error

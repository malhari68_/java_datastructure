class EqualsDemo{

	public static void main(String[]args){
	
		String str1 = "Shashi";
		String str2 = new String("Shashi");
		
		System.out.println("Str1 Equals To Str2 = " + str1.equals(str2));
	}
}

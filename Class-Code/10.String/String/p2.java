class StringDemo{

	public static void main(String[]args){
	
		String str1 = "Core2Web";
		String str2 = new String("Core2Web");

		System.out.println("Str1 = " + System.identityHashCode(str1));
		System.out.println("Str2 = " + System.identityHashCode(str2));

		String str3 = "Core2Web";
		String str4 = new String("Core2Web");

		System.out.println("Str3 = " + System.identityHashCode(str3));
                System.out.println("Str4 = " + System.identityHashCode(str4));				
	}
}

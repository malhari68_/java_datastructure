class HashCodeDemo{

	public static void main(String[]args){
	
		String str1 = "Shashi";
		String str2 = new String("Shashi");
		String str3 = "Shashi";
		String str4 = new String("Shashi");

		System.out.println("Str1 = " + System.identityHashCode(str1));
		System.out.println("Str2 = " + System.identityHashCode(str2));
		System.out.println("Str3 = " + System.identityHashCode(str3));
		System.out.println("Str4 = " + System.identityHashCode(str4));
	}
}

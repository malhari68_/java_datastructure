class StringDemo{

	public static void main(String[]args){
	
		String str1 = "Santosh";
		String str2 = "Doiphode";
		
		System.out.println(str1+str2);

		String str3 = "SantoshDoiphode";
		String str4 = str1 + str2;

		System.out.println("str3 = " + System.identityHashCode(str3));
		System.out.println("Str4 = " + System.identityHashCode(str4));
	}
}

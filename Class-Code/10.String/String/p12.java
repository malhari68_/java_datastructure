//Give two String from user check length of that string and print.

import java.io.*;
class StringDemo{

	static int mystrlen(String str){
	
		char ch[] = str.toCharArray();
		int count = 0;
		for(int i = 0;i < ch.length;i++){
		
			count++;
		}

		return count;
	}

	public static void main(String[]args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter any String :");
		String str1 = br.readLine();
		String str2 = br.readLine();

		int n = mystrlen(str1);
		int m = mystrlen(str2);

		if(m == n){
		
			System.out.println("String are equal");
		}else{
		
			System.out.println("String not equal"); 
		}
	}
}

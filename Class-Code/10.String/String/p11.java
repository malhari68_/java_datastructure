class ToCharArrayDemo{

	static int mystrlen(String str){
	
		char arr[] = str.toCharArray();
		int count = 0;
		for(int i = 0;i < arr.length;i++){
		
			count++;
		}

		return count;
	}

	public static void main(String[]args){
	
		String str = "SantoshDoiphode";
		System.out.println("Str Length = " + str.length());
		int len = mystrlen(str);
		System.out.println("Str Length = " + len);
	}
}

class SBDemo{

	public static void main(String[]args){
	
		String str1 = "Shashi";
		String str2 = new String("Bagal");
		StringBuffer str3 = new StringBuffer("Core2Web");
		
		//String str4 = str1.append(str3);           //can not find Sysmbol
		//String str5 = str3.append(str1);           //StringBuffer can not converted to String
		
		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
		//System.out.println(str4);
		//System.out.println(str5);

		StringBuffer str6 = str3.append(str1);
		System.out.println(str6);
	}
}

class SBDemo{

	public static void main(String[]args){
	
		String str1 = "Shashi";
		String str2 = new String("Bagal");
		StringBuffer str3 = new StingBuffer("Core2Web");

		String str4 = str1.concat(str3);
		StringBuffer str5 = str3.append(str2);

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
		System.out.println(str4);
		System.out.println(str5);
	}
}

//OP :
//p9.java:7: error: cannot find symbol
//                StringBuffer str3 = new StingBuffer("Core2Web");
//                                        ^
//  symbol:   class StingBuffer
//  location: class SBDemo
//p9.java:9: error: incompatible types: StringBuffer cannot be converted to String
//                String str4 = str1.concat(str3);
//                                          ^
//Note: Some messages have been simplified; recompile with -Xdiags:verbose to get full output
//2 errors

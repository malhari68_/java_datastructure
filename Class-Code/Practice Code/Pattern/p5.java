// 5) Pattern
//          1 2 3 4
//          1 2 3
//          1 2
//          1


class Demo{

	public static void main(String[]args){
	
		int N = 4;
		for(int i = 1;i <= N;i++){
	
		        int num = 1;	
			for(int j = N;j >= i;j--){
			
				System.out.print(num + " ");
				num++;
			}

			System.out.println();
		}
	}
}

// 7) Pattern 
//          A
//          B B
//          C C C
//          D D D D


class Demo{

	public static void main(String[]args){
	
		int N = 4;
	        char c = 'A';
		for(int i = 1;i <= N;i++){
		
			for(int j = 1;j <= i;j++){
			
				System.out.print(c + " ");
			}

			c++;
			System.out.println();
		}
	}
}

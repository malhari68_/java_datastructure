// 8) Pattern
//          E E E E E
//          D D D D D
//          C C C C C
//          B B B B B
//          A A A A A

class Demo{

	public static void main(String[]args){
	
		int N = 5;
		char c = 'E';
		for(int i = 1;i <= N;i++){
		
			for(int j = 1;j <= N;j++){
			
				System.out.print(c + " ");
			}

			c--;
			System.out.println();
		}
	}
}

// 5) Pattern
//           5 4 3 2 1 
//           5 4 3 2 1
//           5 4 3 2 1
//           5 4 3 2 1
//           5 4 3 2 1

class Demo{

	public static void main(String[]args){
	
		int N = 5;
		for(int i = 1;i <= N;i++){
		
			int num = 5;
			for(int j = 1;j <= N;j++){
			
				System.out.print(num + " ");
				num--;
			}

			System.out.println();
		}
	}
}

// 6) Pattern
//           A A A A A
//           B B B B B
//           C C C C C
//           D D D D D
//           E E E E E

class Demo{

	public static void main(String[]args){
	
		int N = 5;
		char c = 'A';
		for(int i = 1;i <= N;i++){
		
			for(int j = 1;j <= N;j++){
			
				System.out.print(c + " ");
			}

			c++;
			System.out.println();
		}
	}
}

// 9) Pattern
//          E D C B A 
//          E D C B A
//          E D C B A
//          E D C B A 
//          E D C B A

class Demo{

	public static void main(String[]args){
	
		int N = 5;
		for(int i = 1;i <= N;i++){
		
			char c = 'E';
			for(int j = 1;j <= N;j++){
			
				System.out.print(c + " ");
				c--;
			}

			System.out.println();
		}
	}
}

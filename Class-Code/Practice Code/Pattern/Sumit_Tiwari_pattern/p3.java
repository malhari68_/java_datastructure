// 3) Pattern
//           1 2 3 4 5
//           1 2 3 4 5
//           1 2 3 4 5 
//           1 2 3 4 5
//           1 2 3 4 5

class Demo{

	public static void main(String[]args){
	
		int N = 5;
		for(int i = 1;i <= N;i++){
		
			int num = 1;
			for(int j = 1;j <= N;j++){
			
				System.out.print(num + " ");
				num++;
			}

			System.out.println();
		}
	}
}

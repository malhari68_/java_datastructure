// 4) Pattern
//          5 5 5 5 5
//          4 4 4 4 4
//          3 3 3 3 3
//          2 2 2 2 2
//          1 1 1 1 1

class Demo{

	public static void main(String[]args){
	
		int N = 5;
		int num = 5;
		for(int i = 1;i <= N;i++){
		
			for(int j = 1;j <= N;j++){
			
				System.out.print(num + " ");
			}

			num--;
			System.out.println();
		}
	}
}

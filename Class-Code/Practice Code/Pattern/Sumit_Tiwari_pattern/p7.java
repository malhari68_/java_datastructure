// 7) Pattern
//          A B C D E 
//          A B C D E
//          A B C D E
//          A B C D E
//          A B C D E

class Demo{

	public static void main(String[]args){
	
		int N = 5;
		for(int i = 1;i <= N;i++){
		
			char c = 'A';
			for(int j = 1;j <= N;j++){
			
				System.out.print(c + " ");
				c++;
			}

			System.out.println();
		}
	}
}

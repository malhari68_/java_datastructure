// 16) Pattern
//          A B C D
//          A B C
//          A B
//          AA B C D
//          A B C
//          A B
//          A


class Demo{

	public static void main(String[]args){

	        int N = 4;
	        for(int i = 1;i <= N;i++){
	     
			char c = 'A';
			
			for(int j = N;j >= i;j--){
				
				System.out.print(c + " ");
			        c++;
			}

		        System.out.println();
		}		
	}
}

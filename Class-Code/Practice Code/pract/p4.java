class Demo{

	public static void main(String[]args){
	
		int sellingPrice = 1200;
		int costPrice = 1200;

		if(sellingPrice == costPrice){
		
			System.out.println("Neither profit nor loss");
		}else if(sellingPrice > costPrice){
		
			int profit = sellingPrice - costPrice;
			System.out.println("Profit of : " + profit);
		}else{
		
			int loss = costPrice - sellingPrice;
			System.out.println("Loss of : " + loss);
		}
	}
}

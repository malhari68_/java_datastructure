class Demo{

	public static void main(String[]args){
	
		int a = 3;
		int b = 5;
		int c = 5;

		if(a*a + b*b == c*c || b*b + c*c == a*a || c*c + a*a == b*b){
		
			System.out.println("Triangle is a pythagorous triplet");
		}else{
		
			System.out.println("Triangle is not a pythagorous triplet");
		}
	}
}

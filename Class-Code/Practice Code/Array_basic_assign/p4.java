import java.io.*;
class Demo{

	public static void main(String[]args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter T : ");
		int T = Integer.parseInt(br.readLine());
		System.out.println("Enter N : ");
		int N = Integer.parseInt(br.readLine());
		System.out.println("Enter Elements : ");
		int arr[] = new int[N];
		int sum = 1;
		int res = 0;
		for(int i = 0;i < arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
			sum = sum * arr[i];
			res = sum % T;
		}
		
		System.out.println("Product is : " + sum);
	}
}

import java.io.*;
import java.util.*;
class Demo{

	public static void main(String[]args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter N1 : ");
		int N1 = Integer.parseInt(br.readLine());
		int arr1[] = new int[N1];
		System.out.println("Enter Elements : ");
		int res1 = 0;
		for(int i = 0;i < arr1.length;i++){
		
			arr1[i] = Integer.parseInt(br.readLine());
		}

		Arrays.sort(arr1);
		for(int i = 0;i < arr1.length;i++){
		
			res1 = arr1[arr1.length - 1];
		}
		System.out.println("Maximum element in arr1 : " + res1);
		
		System.out.println("Enter N2 : ");
		int N2 = Integer.parseInt(br.readLine());
		int arr2[] = new int[N2];
		System.out.println("Enter Elements : ");
		int res2 = 0;
		for(int i = 0;i < arr2.length;i++){
		
			arr2[i] = Integer.parseInt(br.readLine());
		}
		
		Arrays.sort(arr2);
		for(int i = 0;i < arr2.length;i++){
		
			res2 = arr2[0];
		}
		System.out.println("Minimum element in arr2 : " + res2);

		int product = (res1) * (res2);
		System.out.println("Product => " + product);
	}
}

import java.io.*;
import java.util.*;
class Demo{

	public static int fun(int arr2[]){
	
		Arrays.sort(arr2);
		return arr2[0];
	}

	public static int run(int arr1[],int N1){
	
		Arrays.sort(arr1);
		return arr1[N1-1];
	}

	public static void main(String[]args)throws IOException{
	 
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter N1 : ");
		int N1 = Integer.parseInt(br.readLine());
		System.out.println("Enter Elements : ");
		int arr1[] = new int[N1];
		for(int i = 0;i < arr1.length;i++){	
		
			arr1[i] = Integer.parseInt(br.readLine());
		}
		
		int res1 = run(arr1,N1);
		//System.out.println("Maximum : " + res1); 
		
		System.out.println("Enter N2 : ");
		int N2 = Integer.parseInt(br.readLine());
		System.out.println("Enter Elements : ");
		int arr2[] = new int[N2];
		for(int i = 0;i < arr2.length;i++){	
		
			arr2[i] = Integer.parseInt(br.readLine());
		}

		int res2 = fun(arr2);
		//System.out.println("Maximum : " + res2);
		
		System.out.println("The product of "+ res1 + " and " + res2 + " is " + res1 * res2);
	}
}

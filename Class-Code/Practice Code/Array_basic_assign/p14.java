import java.io.*;
import java.util.*;
class Demo{

	public static void main(String[]args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter N : ");
		int N = Integer.parseInt(br.readLine());
		System.out.println("Enter K : ");
		int K = Integer.parseInt(br.readLine());
		int arr1[] = new int[N];
		int arr2[] = new int[N];
		System.out.println("Enter Elements : ");
		int count = 0;
		int res = 0;
		for(int i = 0;i < arr1.length;i++){
		
			arr1[i] = Integer.parseInt(br.readLine());
			arr2[i] = arr1[i];
		}

		for(int i = 0;i < arr1.length;i++){
		
			for(int j = 0;j < arr1.length;j++){
			
				if(arr1[i] == arr1[j]){
				
					count++;
				}
			}

			if(K == count){
			
				res = arr1[i];
			}else{
			
				Arrays.sort(arr2);
				res = arr2[0];
			}

			count = 0;
		}

		System.out.println(res);
	}
}

import java.io.*;
import java.util.*;
class Demo{

	public static void main(String[]args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter N1 : ");
		int N1 = Integer.parseInt(br.readLine());
		int arr1[] = new int[N1];
		System.out.println("Enter Elements : ");
		for(int i = 0;i < arr1.length;i++){
		
			arr1[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter sum : ");
		int K = Integer.parseInt(br.readLine());
		boolean found = false;
		
		for(int i = 0;i < arr1.length;i++){
		
			for(int j = i + 1;j < arr1.length;j++){
			        
				if(arr1[i] + arr1[j] == K){
					found = true;
				        break;	
				}
			}
			if(found)
				break;
		}
		if(found){
		
			System.out.println("Yes");
		}else{
		
			System.out.println("No");
		}
	}
}

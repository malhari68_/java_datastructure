import java.io.*;
class Demo{

	public static void main(String[]args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter N : ");
		int N = Integer.parseInt(br.readLine());
		System.out.println("Enter Elements : ");
		int arr[] = new int[N];
		for(int i = 0;i < arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		int res = arr[0];
		for(int i = 0;i < arr.length;i++){
		
			if(res < arr[i]){
			
				res = arr[i];
			}
		}

		System.out.println("Maximum : " + res);
	}
}

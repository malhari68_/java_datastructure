import java.io.*;
import java.util.*;
class Demo{

	public static void main(String[]args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter N1 : ");
		int N1 = Integer.parseInt(br.readLine());
		int arr1[] = new int[N1];
		System.out.println("Enter Elements : ");
		for(int i = 0;i < arr1.length;i++){
		
			arr1[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter sum : ");
		int sum = Integer.parseInt(br.readLine());
		int res = 0;
		int a = 0;
		int b = 0;
		
		for(int i = 0;i < arr1.length;i++){
		
			res = res + arr1[i];
			for(int j = i + 1;j < arr1.length;j++){
			        
				res = res + arr1[j];
				if(res == sum){
					a = i;
					b = j;
				        break;	
				}
			}
			res = 0;
		}
		System.out.println(a + " between " + b);
	}
}

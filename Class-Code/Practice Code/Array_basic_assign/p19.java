import java.io.*;
import java.util.*;
class Demo{

	    static List<Integer> commonElement(int[] arr1, int[] arr2, int[] arr3) {
		    List<Integer> s1 = new ArrayList<>();
		    List<Integer> s2 = new ArrayList<>();
		    List<Integer> s3 = new ArrayList<>();  
		    List<Integer> common = new ArrayList<>();

                    for(int data : arr1) {
			                                                
		    	    s1.add(data);
		    }	

		    for(int data : arr2) {

			    s2.add(data);
		    }
		
		    for(int data : arr3) {

			    s3.add(data);
		    }

		    for(int element : s1) {
			                                                                          
		      	    if(s2.contains(element) && s3.contains(element)) {
				    common.add(element);
			    }
		    }
		    return common;
	    }

	public static void main(String[]args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter N1 : ");
		int N1 = Integer.parseInt(br.readLine());
		int arr1[] = new int[N1];
		System.out.println("Enter Elements in arr1 : ");
		for(int i = 0;i < arr1.length;i++){
		
			arr1[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter N2 : ");
		int N2 = Integer.parseInt(br.readLine());
		int arr2[] = new int[N2];
		System.out.println("Enter Elements in arr2 : ");
		for(int i = 0;i < arr2.length;i++){
		
			arr2[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter N3 : ");
		int N3 = Integer.parseInt(br.readLine());
		int arr3[] = new int[N3];
		System.out.println("Enter Elements in arr3 : ");
		for(int i = 0;i < arr3.length;i++){
		
			arr3[i] = Integer.parseInt(br.readLine());
		}

		List<Integer> commonElemenets = commonElement(arr1, arr2, arr3);
		if(!commonElemenets.isEmpty()) {
			
			System.out.println("Common elements are : " + commonElemenets);
		}else {
			
			System.out.println(" No Common element found ");
		}   
	}
}

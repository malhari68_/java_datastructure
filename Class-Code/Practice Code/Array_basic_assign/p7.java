import java.io.*;
import java.util.*;
class Demo{

	public static void main(String[]args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter N : ");
		int N = Integer.parseInt(br.readLine());
		System.out.println("Enter Elements : ");
		int arr[] = new int[N];
		for(int i = 0;i < arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

	        Arrays.sort(arr);
		for(int i = arr.length - 1;i >= 0;i--){
		
			System.out.print(arr[i]);
		}

		System.out.println();
	}
}

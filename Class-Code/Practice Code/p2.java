class Parent{

	int x = 10;
	void m1(){
	
		System.out.println("In Parent-M1");
	}
}
class Child extends Parent{

	int a = 20;
	void m1(){
	
		System.out.println("In Child-M1");
	}
}
class Demo{

	Demo(Parent P){
	
		System.out.println("In Constructor-Parent");
		P.m1();
	}
	Demo(Child C){
	
		System.out.println("In Constructor-Child");
		C.m1();
	}
	public static void main(String[]args){
	
		Demo obj1 = new Demo(new Parent());
		Demo obj2 = new Demo(new Child());
	}
}

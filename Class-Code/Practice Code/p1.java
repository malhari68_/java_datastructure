class Parent{

	Parent(){
	
		System.out.println("In Parent Constructor");
	}

	int x = 10;
	static int y = 20;
	void m1(){
	
		System.out.println("In M1");
	}

	static void m2(){
	
		System.out.println("In M2");
	}
}
class Child extends Parent{

	Child(){
	
		System.out.println("In Child");
	}

	static void m3(){
	
		System.out.println("In M3");
	}
}
class Client{

	public static void main(String[]args){
	
		Child obj = new Child();
		obj.m1();
		obj.m2();
		obj.m3();
	}
}

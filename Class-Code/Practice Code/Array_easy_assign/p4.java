import java.io.*;
class Demo{

	public static void main(String[]args)throws IOException{	

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter K : ");
		int K = Integer.parseInt(br.readLine());
		System.out.println("Enter N : ");
		int N = Integer.parseInt(br.readLine());
		int arr[] = new int[N];
		System.out.println("Enter Elements : ");
		for(int i = 0;i < arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

	        int count = 0;
		for(int i = 0;i < arr.length;i++){
		
			for(int j = arr.length - 1;j > i;j--){
			
				if((arr[i] + arr[j]) == K){
				
					count++;
				}
			}
		}

		System.out.println("Total count is : " + count);
	}
}

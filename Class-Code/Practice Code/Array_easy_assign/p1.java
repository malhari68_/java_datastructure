import java.io.*;
class Demo{

	public static void main(String[]args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter Range : ");
		int N = Integer.parseInt(br.readLine());
		System.out.println("Enter Elements : ");
		int arr[] = new int[N - 1];
		for(int i = 0;i < arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		int size = arr.length;
		int sum = ((size+1) * (size+2)) / 2;
		for(int i = 0;i < size;i++){
		
			sum = sum - arr[i];
		}

		System.out.println("Missing Element: " + sum);
	}
}

import java.io.*;
class Demo{

	public static void main(String[]args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter N : " );
		int N = Integer.parseInt(br.readLine());
		System.out.println("Enter Elements : " );
		int arr[] = new int[N];
		for(int i = 0;i < arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		int count = 0;
		int sum = arr[0];
		for(int i = 0;i < arr.length;i++){
		
			if(sum < arr[i]){
			
				count++;
				sum = arr[i];
			}
		}

		if(count >= 1){
		
			System.out.println("Peak Element(1)");
		}else{
		
			System.out.println("Not Found Peak Element(0)");
		}
	}
}

// the Arrays.compare method is supported in Java versions starting from Java 9 and later
import java.io.*;
import java.util.*;
import java.util.Arrays;
class Demo{

	public static void main(String[]args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter N : " );
		int N = Integer.parseInt(br.readLine());
		System.out.println("Enter Elements : " );
		int arr1[] = new int[N];
		int arr2[] = new int[N];
		for(int i = 0;i < arr1.length;i++){
		
			arr1[i] = Integer.parseInt(br.readLine());
			arr2[i] = arr1[i];
		}
		
		Arrays.sort(arr1);
		System.out.println("Result is : " + Arrays.compare(arr1,arr2));
	}
}
//OP =>
//       true = 0;
//       false = -1;

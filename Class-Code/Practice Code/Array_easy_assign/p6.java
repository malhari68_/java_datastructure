import java.io.*;
import java.util.*;
class Demo{

	public static void main(String[]args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter N : " );
		int N = Integer.parseInt(br.readLine());
		System.out.println("Enter Elements : " );
		int arr[] = new int[N];
		for(int i = 0;i < arr.length;i++){
		
			arr[i] = Integer.parseInt(br.readLine());
		}

		Arrays.sort(arr);
		int secondLargest = arr[N - 2];
		System.out.println("The second largest distinct element is : " + secondLargest);
	}
}

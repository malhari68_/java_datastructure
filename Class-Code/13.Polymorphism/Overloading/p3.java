class Demo{

	void fun(int x,float y){
	
		System.out.println("Int-Float Para");
	}

	void fun(float x,int y){
	
		System.out.println("Float-Int Para");
	}
}
class Client{

	public static void main(String[]args){
	
		Demo obj = new Demo();
		obj.fun(10,10);
	}
}
//OP :
/*p3.java:18: error: reference to fun is ambiguous
                obj.fun(10,10);
                   ^
  both method fun(int,float) in Demo and method fun(float,int) in Demo match
1 error*/

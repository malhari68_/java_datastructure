class Demo{

        void fun(Object obj){                      //String str = null

                System.out.println("Object");
        }

        void fun(String str){              //StringBuffer str1 = null

                System.out.println("String");
        }
}
class Client{

        public static void main(String[]args){

                Demo obj = new Demo();
                obj.fun("Core2Web");
                obj.fun(new StringBuffer("Core2Web"));
                obj.fun(null);
        }
}

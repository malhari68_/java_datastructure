//Overloding/Early Binding/Compile time polymorphism
//Overriding/Late Binding/Runtime Polymorphism


class Parent{

	String fun(){
	
		return new String();
	}
}
class Child extends Parent{

	Object fun(){
	
		return new Object();
	}
}
//OP :
/*p13.java:10: error: fun() in Child cannot override fun() in Parent
        Object fun(){
               ^
  return type Object is not compatible with String
1 error*/

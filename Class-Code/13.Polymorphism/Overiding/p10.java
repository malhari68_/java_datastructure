class Parent{

        int fun(){

                System.out.println("Parent Fun");
                return 10;
        }
}
class Child extends Parent{

        char fun(){

                System.out.println("Child Fun");
                return 'A';
        }
}
class Client{

        public static void main(String[]args){

                Parent c = new Child();
                c.fun();
        }
}
//OP : 
/*p10.java:11: error: fun() in Child cannot override fun() in Parent
        char fun(){
             ^
  return type char is not compatible with int
1 error*/

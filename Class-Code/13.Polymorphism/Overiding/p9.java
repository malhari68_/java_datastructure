class Parent{

        int fun(){

                System.out.println("Parent Fun");
		return 10;
        }
}
class Child extends Parent{

        int fun(){

                System.out.println("Child Fun");
		return 20;
        }
}
class Client{

        public static void main(String[]args){

                Child c = new Child();
                c.fun();
        }
} 

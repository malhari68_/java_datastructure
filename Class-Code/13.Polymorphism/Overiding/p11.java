class Parent{

	Object fun(){
	
		Object obj = new Object();
                System.out.println("In Parent");
                return obj;                  //return new object
	}
}
class Child extends Parent{

	String fun(){
	
		System.out.println("Child Fun");
		return "shashi";
	}
}
class Client{

	public static void main(String[]args){
	
		Parent p = new Child();
		p.fun();
	}
}

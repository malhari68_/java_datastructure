class Parent{

        Parent(){

                System.out.println("Parent Constructor");
        }

        void fun(){

                System.out.println("In Fun");
        }
}
class Child extends Parent{

        Child(){

                System.out.println("Child Constructor");
        }

        void gun(){

                System.out.println("In Gun");
        }
}
class Client{

        public static void main(String[]args){

		Parent obj1 = new Child();
		Child obj2 = new Parent();
        }
}
//OP :
/*p5.java:30: error: incompatible types: Parent cannot be converted to Child
                Child obj2 = new Parent();
                             ^
1 error*/

//error pn nahi ani overriding chi concept pn nahi => static ha eka ch class la bind asto


class Parent{

        static void fun(){

                System.out.println("Parent Fun");
        }
}
class Child extends Parent{

        static void fun(){

                System.out.println("Child Fun");
        }
}
class Client{

	public static void main(String[]args){
	
		Parent obj1 = new Parent();
		obj1.fun();

		Child obj2 = new Child();
		obj2.fun();

		Parent obj3 = new Child();
		obj3.fun();
	}
}

class Parent{

	Parent(){
	
		System.out.println("Parent Constructor");
	}

	void Property(){
	
		System.out.println("Home,Car,Gold");
	}

	void Marry(){
	
		System.out.println("Deepika Padukone");
	}
}
class Child extends Parent{

	Child(){
	
		System.out.println("Child Constructor");
	}

	void Marry(){
	
		System.out.println("Kriti Shetty");
	}
}
class Client{

	public static void main(String[]args){
	
		Child obj = new Child();
		obj.Property();
		obj.Marry();
	}
}

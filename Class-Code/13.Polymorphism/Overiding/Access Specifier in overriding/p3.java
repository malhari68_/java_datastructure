class Parent{

        private void fun(){                       //Access Specifier : private

                System.out.println("Parent Fun");
        }
}
class Child extends Parent{

        void fun(){                            //Access Specifier : default

                System.out.println("Child Fun");
        }
}

//override vr impact padnar nahi bcz => Parent chi method class madhe inherit hot nahi(private fakt class madhech access hoto)

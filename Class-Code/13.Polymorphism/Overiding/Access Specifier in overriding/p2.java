class Parent{

        void fun(){                       //Access Specifier : default

                System.out.println("Parent Fun");
        }
}
class Child extends Parent{

        public void fun(){                            //Access Specifier : public

                System.out.println("Child Fun");
        }
}

//Parent cha access Specifier chota asla tari chalel but child cha access Specifier mota asla pahije


class Parent{

        void fun(){                       //Access Specifier : default

                System.out.println("Parent Fun");
        }
}
class Child extends Parent{

        private void fun(){                            //Access Specifier : private

                System.out.println("Child Fun");
        }
}
//OP :
/*p4.java:10: error: fun() in Child cannot override fun() in Parent
        private void fun(){                            //Access Specifier : private
                     ^
  attempting to assign weaker access privileges; was package
1 error*/

class Parent{

	public void fun(){                       //Access Specifier : public
	
		System.out.println("Parent Fun");
	}
}
class Child extends Parent{

	void fun(){                            //Access Specifier : default
	
		System.out.println("Child Fun");
	}
}
//OP :
/*p1.java:10: error: fun() in Child cannot override fun() in Parent
        void fun(){                            //Access Specifier : default
             ^
  attempting to assign weaker access privileges; was public
1 error*/

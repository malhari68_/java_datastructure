class Parent{

        Parent(){

                System.out.println("Parent Constructor");
        }

        void Fun(){

                System.out.println("In Parent Fun");
        }
}
class Child extends Parent{

        Child(){

                System.out.println("Child Constructor");
        }

        void Fun(int x){

                System.out.println("In Child Fun");
        }
}
class Client{

        public static void main(String[]args){

                Parent obj1 = new Child();
                obj1.Fun(10);
        }
}

//OP :
/*p6.java:30: error: method Fun in class Parent cannot be applied to given types;
                obj1.Fun(10);
                    ^
  required: no arguments
  found: int
  reason: actual and formal argument lists differ in length
1 error*/

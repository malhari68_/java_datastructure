class Parent{

	Parent(){
	
		System.out.println("Parent Constructor");
	}

	void fun(){
	
		System.out.println("In Fun");
	}
}
class Child extends Parent{

	Child(){
	
		System.out.println("Child Constructor");
	}

	void gun(){
	
		System.out.println("In Gun");
	}
}
class Client{

	public static void main(String[]args){
	
		Child obj1 = new Child();
		obj1.fun();
		obj1.gun();

		Parent obj2 = new Parent();
		obj2.fun();
		obj2.gun();                  //Parent cya reference vr child chi method call hot nahi (method table cha vapar compilier karto)
	}
}
//OP :
/*p4.java:35: error: cannot find symbol
                obj2.gun();
                    ^
  symbol:   method gun()
  location: variable obj2 of type Parent
1 error*/

class Parent{

        void fun(){

                System.out.println("Parent Fun");
        }
}
class Child extends Parent{

        static void fun(){

                System.out.println("Child Fun");
        }
}
//OP :
/*static2.java:10: error: fun() in Child cannot override fun() in Parent
        static void fun(){
                    ^
  overriding method is static
1 error*/

class Parent{

        static void fun(){

                System.out.println("Parent Fun");
        }
}
class Child extends Parent{

        void fun(){

                System.out.println("Child Fun");
        }
}
//OP :
/*static1.java:10: error: fun() in Child cannot override fun() in Parent
        void fun(){
             ^
  overridden method is static
1 error*/

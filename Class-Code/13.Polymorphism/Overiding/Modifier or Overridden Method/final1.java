class Parent{

        final void fun(){

                System.out.println("Parent Fun");
        }
}
class Child extends Parent{

        void fun(){

                System.out.println("Child Fun");
        }
}
//OP :
/*final.java:10: error: fun() in Child cannot override fun() in Parent
        void fun(){
             ^
  overridden method is final
1 error*/

class Parent{

	void fun(){
	
		System.out.println("Parent Fun");
	}
}
class Child extends Parent{

	void fun(){
	
		System.out.println("Child Fun");
	}

	void gun(){
	
		
	}
}
class Client{

	public static void main(String[]args){
	
		Parent p = new Parent();
		p.fun();
		//p.gun()       //Can Not find Symbol
                
		Child c = new Child();
		c.fun();
		c.gun();
	}
}

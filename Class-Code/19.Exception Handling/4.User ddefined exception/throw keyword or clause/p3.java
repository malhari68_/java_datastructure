import java.util.Scanner;
class RainException extends RuntimeException{

	RainException(String msg){
	
		super(msg);
	}
}
class ColdException extends RuntimeException{

	ColdException(String msg){
	
		super(msg);
	}
}
class Core2Web{

	public static void main(String[]args){
	
		Scanner sc = new Scanner(System.in);
		String data1 = "rainy";
		String data2 = "cold";
		System.out.println("Enter current environment :");
		String env = sc.nextLine();
		if(env.equals(data1))
                       throw new RainException("Chatri geun class la jane");
		
		if(env.equals(data2))
		       throw new ColdException("Swatter galne");

		System.out.println("Envirnment = " + env);	
	}
}

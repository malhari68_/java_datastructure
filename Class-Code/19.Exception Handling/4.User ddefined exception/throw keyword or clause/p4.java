import java.util.Scanner;

class DataOverFlowException extends RuntimeException {
    DataOverFlowException(String msg) {
        super(msg);
    }
}

class DataUnderFlowException extends RuntimeException {
    DataUnderFlowException(String msg) {
        super(msg);
    }
}

class Client {
    public static void main(String[] args) {

        int arr[] = new int[5];
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter integer value:");
        System.out.println("Note: Value should be between 0 and 100");
        for (int i = 0; i < arr.length; i++) {
            int data = sc.nextInt();

            try {
                if (data < 0) {
                    throw new DataUnderFlowException("Data is smaller than 0.");
                }
            } catch (DataUnderFlowException obj) {
		obj.printStackTrace();
                continue;
            }

            try {
                if (data > 100) {
                    throw new DataOverFlowException("Data is greater than 100.");
                }
            } catch (DataOverFlowException obj) {
		obj.printStackTrace();
                continue;
            }

            arr[i] = data;
        }

        System.out.println("Stored values in the array:");
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i] + " ");
        }
    }
}

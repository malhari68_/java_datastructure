class Demo{

	void m1(){
	
		System.out.println("In M1");
		System.out.println(10/0);
		m2();
	}
	void m2(){
	
		System.out.println("In M2");
	}
	public static void main(String[]args){
	
		System.out.println("Start Main");
		Demo obj = new Demo();
		obj.m1();
		System.out.println("End Main");
	}
}
/*Start Main
In M1
Exception in thread "main" java.lang.ArithmeticException: / by zero
        at Demo.m1(p1.java:6)
        at Demo.main(p1.java:17)*/

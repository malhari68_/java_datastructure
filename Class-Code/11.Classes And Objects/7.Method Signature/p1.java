class Demo{

	int x = 10;

	Demo(){
	
		System.out.println("In Constructor");
		System.out.println("X = " + x);
	}

	Demo(){
	
		System.out.println("In Constructor");
		System.out.println("X = " + x);
	}
}

//OP :
/*p1.java:11: error: constructor Demo() is already defined in class Demo
        Demo(){
        ^
1 error*/

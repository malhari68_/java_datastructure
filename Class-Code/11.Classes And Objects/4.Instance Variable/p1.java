class Demo{

	int x = 10;          //global non-static variable
	static int y = 10;   //class variable/globle static variable

	void fun(){          //instance method
	
		System.out.println(x);
		System.out.println(y);
	}
}

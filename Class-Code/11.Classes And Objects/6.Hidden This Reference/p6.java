class Demo{

	int x = 10;

	Demo(){                                //Demo(Demo this)
	
		this(10);                      //Demo(int x) la call karto
		System.out.println("In No-Args Constructor");
	}

	Demo(int x){                            //Demo(Demo this,int x)
	
		super();                        //Demo(int x) cya parent class la call karto
		System.out.println("In Para Constructor");
	}

	public static void main(String[]args){
	
		Demo obj = new Demo(20);           //Demo(obj,20)
	}
}

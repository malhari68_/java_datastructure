class Demo{

	int x = 10;

	Demo(){                             //Demo(Demo this)
	
		System.out.println("In No-Args Constructor");
	}

	Demo(int y){                        //Demo(Demo this,int y)
	
		this();                     //Demo() method la Call Jato
		System.out.println("In Para Constructor");
	}

	public static void main(String[]args){
	
		Demo obj1 = new Demo();     //Demo(obj1)
		
		System.out.println("---------------------------------");

		Demo obj2 = new Demo(50);   //Demo(obj2,50)
	}
}

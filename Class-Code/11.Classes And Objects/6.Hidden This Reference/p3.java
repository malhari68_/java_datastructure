class Demo{

	int x = 20;

	Demo(){
	
		System.out.println("In No-Args Constructor");
	}

	Demo(int x){
	
		System.out.println(this.x);
		System.out.println(x);
		System.out.println("In Para Constructor");
	}

	public static void main(String[]args){
	
		Demo obj1 = new Demo();
		Demo obj2 = new Demo(10);
	}
}

class Demo{

	Demo(){
	
		this(70);
		System.out.println("In No-Args Constructor");
	}

	Demo(int x){
	
		this();
		System.out.println("In Para Constructor");
	}
}
//OP :
/*p5.java:9: error: recursive constructor invocation
        Demo(int x){
        ^
1 error*/

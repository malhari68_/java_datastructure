class Demo{

	Demo(){
	
		System.out.println("No Args");
	}

	Demo(int x){
	
		System.out.println("Args");
	}

	Demo(Demo xyz){
	
		System.out.println("Para Demo");
	}

	public static void main(String[]args){
	
		Demo obj1 = new Demo();           //No Args
		Demo obj2 = new Demo(10);         //Args
		Demo obj3 = new Demo(obj1);       //Para Demo
	}
}

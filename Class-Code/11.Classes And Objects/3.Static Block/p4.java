class Demo{

	static int x = 10;

	static{
	
		static int y = 20;
	}

	void fun(){
	
		static int z = 20;
	}

	static void gun(){
	
		static int a = 20;
	}
}

//OP :
/*p4.java:7: error: illegal start of expression
                static int y = 20;
                ^
p4.java:10: error: class, interface, or enum expected
        void fun(){
        ^
p4.java:13: error: class, interface, or enum expected
        }
        ^
p4.java:18: error: class, interface, or enum expected
        }
        ^
4 errors*/

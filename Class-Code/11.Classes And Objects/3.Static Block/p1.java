class Demo{
        
	static int x = 10;

	static{
	
		System.out.println("Static Block");
		System.out.println(x);
	}
}

//OP :
/*class Demo {
  static int x;

  Demo();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: return

  static {};
    Code:
       0: bipush        10
       2: putstatic     #2                  // Field x:I
       5: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
       8: ldc           #4                  // String Static Block
      10: invokevirtual #5                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      13: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
      16: getstatic     #2                  // Field x:I
      19: invokevirtual #6                  // Method java/io/PrintStream.println:(I)V
      22: return
}*/

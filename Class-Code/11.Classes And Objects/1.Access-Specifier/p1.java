class Demo{

	int x = 10;
	private int y = 20;

	void fun(){
	
		System.out.println(x);
		System.out.println(y);
	}
}
class MainDemo{

	public static void main(String[]args){
	
		Demo obj = new Demo();
		obj.fun();
		System.out.println(obj.x);
		System.out.println(obj.y);
		System.out.println(x);
		System.out.println(y);
	}
}

//O/P :
/*p1.java:19: error: y has private access in Demo
                System.out.println(obj.y);
                                      ^
p1.java:20: error: cannot find symbol
                System.out.println(x);
                                   ^
  symbol:   variable x
  location: class MainDemo
p1.java:21: error: cannot find symbol
                System.out.println(y);
                                   ^
  symbol:   variable y
  location: class MainDemo
3 errors*/

class Core2Web{

	static boolean e_board = true;
	int Stud_Mobile = 400;
	String name = "ShashiSir";

	void classInfo(){
	
		System.out.println("Class E-Board = " + e_board);
		System.out.println("Name = " + name);
		System.out.println("Student Mobile = " + Stud_Mobile);
	}
}
class MainDemo{

	public static void main(String[]args){
	
		Core2Web obj1 = new Core2Web();
		Core2Web obj2 = new Core2Web();

		obj1.classInfo();
		obj2.classInfo();

		System.out.println("-------------------------------");

		obj2.e_board = false;
		obj2.Stud_Mobile = 500;
		obj2.name = "SachinSir";

		obj1.classInfo();
		obj2.classInfo();
	}
}

class Core2Web{

	int noOfCourses = 8;
	private String favCourse = "CPP";
	
	void disp(){
	
		System.out.println(noOfCourses);
		System.out.println(favCourse);
	}
}
class Student{

	public static void main(String[]args){
	
		Core2Web obj = new Core2Web();
		obj.disp();
		System.out.println(obj.noOfCourses);
		System.out.println(obj.favCourse);
	}
}

//O/p :
//Private.java:19: error: favCourse has private access in Core2Web
//                System.out.println(obj.favCourse);
//                                      ^
//1 error

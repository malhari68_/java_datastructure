class StaticDemo{

	static int x = 10;
	static int y = 20;
}

//OP :
/*class StaticDemo {
  static int x;

  static int y;

  StaticDemo();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: return

  static {};
    Code:
       0: bipush        10
       2: putstatic     #2                  // Field x:I
       5: bipush        20
       7: putstatic     #3                  // Field y:I
      10: return
}*/

class Demo{

	int x = 10;

	Demo(){                     //constructor
	
		System.out.println("Constructor");
	}

	{                          //Instance Block
	
		System.out.println("Instance Block 1");
	}

	public static void main(String[]args){
	
		Demo obj = new Demo();
		System.out.println("Main");
	}

	{
	
		System.out.println("Instance Block 2");
	}
}

//OP : 
/*
 - Static Variable
 - Static Block
 - Static Methods
 - Instance Variable
 - Instance Block
 - Constructor
 - Instance Method*/

package  arithfun;

public class Addition{

	int num1 = 0;
	int num2 = 0;

	public Addition(int num1,int num2){
	
		this.num1 = num1;
		this.num2 = num2;
	}

	public int add(){
	
		return num1 + num2;
	}
}

//1) public class kela tr => .java file ch nav ani class nav same asal pahije

//2) public class asen ani default constructor asen tr =>
/*p1.java:16: error: Addition(int,int) is not public in Addition; cannot be accessed from outside package
                Addition obj = new Addition(a,b);
                               ^
1 error*/

//3) default class ani default constructor asen tr =>
/*p1.java:2: error: Addition is not public in arithfun; cannot be accessed from outside package
import arithfun.Addition;
               ^
p1.java:16: error: cannot access Addition
                Addition obj = new Addition(a,b);
                ^
  bad source file: ./Addition.java
    file does not contain class Addition
    Please remove or make sure it appears in the correct subdirectory of the sourcepath.
2 errors*/

//4) public class , public constructor ani method tr protected asen tr =>
/*p1.java:17: error: add() has protected access in Addition
                System.out.println("Addition : " + obj.add());                                                      ^
1 error*/ 

//5) protected specifier other folder access karnyasatti extends(parent-child relation) karav lagat. (child class ch constructor parameter pass karun super() la call kela pahije


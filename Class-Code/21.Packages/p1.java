import java.util.*;
import arithfun.Addition;

class Client{

        public static void main(String[]args){

                Scanner sc = new Scanner(System.in);

                System.out.println("Enter num1 : " );
                int b = sc.nextInt();

                System.out.println("Enter num2 : ");
                int a = sc.nextInt();

                Addition obj = new Addition(a,b);
                System.out.println("Addition : " + obj.add());
        }
}

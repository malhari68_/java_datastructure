class Parent{

        int x = 10;
        Parent(){

                System.out.println("In Parent Constructor");
        }

        void Access(){

                System.out.println("Parent Instance");
        }
}
class Child extends Parent{

        int y = 20;
        Child(){

                System.out.println("In Child Constructor");
                System.out.println(x);
                System.out.println(y);
        }
}
class Client{

        public static void main(String[]args){

                Child obj = new Child();
                obj.Access();
        }
}

/*  Java can not support multiple inheritance,bcz ambiguity mule chalat nahi*/

/*  Child class cha, object ha indirect parent asto*/

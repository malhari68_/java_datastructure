class Parent{

	int x = 10;
	static int y = 20;

	Parent(){
	
		System.out.println("Parent");
	}
}
class Child extends Parent{

	int x = 100;
	static int y = 200;

	Child(){
	
		System.out.println("Child");
	}

	void Access(){
	
		System.out.println(super.x);
		System.out.println(super.y);
		System.out.println(x);
		System.out.println(y);
	}
}
class Client{

	public static void main(String[]args){
	
		Child obj = new Child();
		obj.Access();
	}
}

  /* super() => constructor madhe 1st line aste*/
  
  /*  super.variable => Parent class ghosti Child class madhe access karnyasatti */

 /* super is non-static variable*/

class Parent{

        static{

                System.out.println("In Parent Static Block");
        }
}
class Child extends Parent{

        static{

                System.out.println("In Child Static Block");
        }
}
class Client{

        public static void main(String[]args){

                Child obj = new Child();
        }
}

/*  extends lavlyane Constructor pahil parent class la call jato => static variable,static block and static method intialize karnyasatti */

/*  child cya constructor madun parent constructor call => instance varible initialize karnyasatti*/

import java.util.*;
class Employee{

	int empId= 0;
	String name = null;

	Employee(int empId,String name){
	
		this.empId = empId;
		this.name = name;
	}

	public String toString(){
	
		return empId + ":" + name;
	}
}
/*class SortByName implements Comparator{

	public int compare(Object obj1,Object obj2){
	
		return ((Employee)obj1).name.compareTo(((Employee)obj2).name);
	}
}*/
class SortById implements Comparator<Employee>{

	public int compare(Employee obj1,Employee obj2){
	
		return (int) (obj1.empId - obj2.empId); 
	}
}
class Demo{

	public static void main(String[]args){
	
		ArrayList al = new ArrayList();
		al.add(new Employee(25,"Kanha"));
		al.add(new Employee(15,"Ashish"));
		al.add(new Employee(22,"Rahul"));
		System.out.println("ArrayList : " + al);
		System.out.println("___________________________________________________________________________________________");

		Collections.sort(al,(obj1,obj2) ->{
		
			return ((Employee)obj1).name.compareTo(((Employee)obj2).name); 
		});       
		System.out.println("ArrayList : " + al);
         
		System.out.println("___________________________________________________________________________________________");
		
		Collections.sort(al,new SortById());
                System.out.println("ArrayList : " + al);
	}
}

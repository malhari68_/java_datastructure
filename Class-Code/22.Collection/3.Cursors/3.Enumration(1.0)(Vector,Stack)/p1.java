import java.util.*;
class CursorDemo{

	public static void main(String[]args){
	
		Vector ve = new Vector();
		ve.add("Rahul");
		ve.add("Kanha");
		ve.add("Ashish");
		ve.add("Badhe");

		Enumeration cursor = ve.elements();
		System.out.println(cursor.getClass().getName());

		while(cursor.hasMoreElements()){
		
			System.out.println(cursor.nextElement());
		}
	}
}

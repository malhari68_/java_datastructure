import java.util.*;
class CursorDemo{

        public static void main(String[]args){

                ArrayList al = new ArrayList();
                al.add("Ashish");
                al.add("Shashi");
                al.add("Kanha");
                al.add("Badhe");

                for(var x : al){

                        System.out.println(x.getClass().getName());
                }

                System.out.println("__________________________________________");

                Iterator cursor = al.iterator();
                while(cursor.hasNext()){

                        if("Kanha".equals(cursor.next())){

                                cursor.remove();
                        }
                }

                System.out.println(al);
        }
}

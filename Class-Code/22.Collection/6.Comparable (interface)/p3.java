import java.util.*;
class Movies implements Comparable{

	String movieName = null;
	float totColl = 0.0f;

	Movies(String movieName,float totColl){
	
		this.movieName = movieName;
		this.totColl = totColl;
	}

	public String toString(){
	
		return "{" + movieName + " : " + totColl + "}"; 
	}

	public int compareTo(Object obj){
	
		return movieName.compareTo(((Movies)obj).movieName);
	}

	/*public int compareTo(Object obj){
	
		return (int) ((totColl) - (((Movies)obj).totColl));
	}*/
}
class TreeSetDemo{

	public static void main(String[]args){
	
		TreeSet ts = new TreeSet();

		ts.add(new Movies("Gadar-2",3150.00f));
		ts.add(new Movies("OMG-2",100.00f));
		ts.add(new Movies("Jailer",250.00f));
		ts.add(new Movies("OMG-2",1220.00f));

		System.out.println(ts);
	}
}

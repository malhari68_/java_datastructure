import java.util.*;

class Myclass implements Comparable<Myclass> {

    String str = null;

    Myclass(String str) {
        this.str = str;
    }

    @Override
    public int compareTo(Myclass other) {
        return this.str.compareTo(other.str);
    }

    public String toString() {
        return str;
    }

    public static void main(String[] args) {
        TreeSet<Myclass> t = new TreeSet<>();
        t.add(new Myclass("kanha"));
        t.add(new Myclass("harshal"));
        t.add(new Myclass("Santosh"));
        t.add(new Myclass("Yogesh"));
        t.add(new Myclass("Dinesh"));

        System.out.println(t);
    }
}

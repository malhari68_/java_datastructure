import java.util.*;
class NavigableSetDemo{

	public static void main(String[]args){
	
		NavigableSet ns = new TreeSet();
		ns.add(10);
		ns.add(30);
		ns.add(14);
		ns.add(27);
		ns.add(23);

		System.out.println("NavigableSet => " + ns);
		System.out.println("___________________________________________________________________________________");
                
	        //E lower(E);
		System.out.println(ns.lower(25));

		System.out.println("___________________________________________________________________________________");
		
		//E floor(E);
		System.out.println(ns.floor(23));

		System.out.println("___________________________________________________________________________________");
		//E ceiling(E);
		System.out.println(ns.ceiling(25));

		System.out.println("___________________________________________________________________________________");
		
		//E higher(E);
		System.out.println(ns.higher(27));

		System.out.println("___________________________________________________________________________________");
		
		//E pollFirst();
		System.out.println(ns.pollFirst());

		System.out.println("___________________________________________________________________________________");
		
		//E pollLast();
		System.out.println(ns.pollLast());

		System.out.println("___________________________________________________________________________________");
		
		//iterator();
		Iterator itr1 = ns.iterator();
		while(itr1.hasNext()){
		
			System.out.println(itr1.next());
		}
		
		System.out.println("___________________________________________________________________________________");
		
		//descendingSet();
		System.out.println(ns.descendingSet());

		System.out.println("___________________________________________________________________________________");
		
		//descendingIterator();
		Iterator itr2 = ns.descendingIterator();
		while(itr2.hasNext()){
		
			System.out.println(itr2.next());
		}

		System.out.println("___________________________________________________________________________________");
		
		//subSet(E, boolean, E, boolean);
		System.out.println(ns.subSet(14,true,27,false));

		//headSet(E, boolean);
		//System.out.println(ns.headSet(E, ));
	}
}

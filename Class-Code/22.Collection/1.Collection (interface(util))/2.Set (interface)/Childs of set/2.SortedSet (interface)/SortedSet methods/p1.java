import java.util.*;
class SortedSetDemo{

	public static void main(String[]args){
	
		SortedSet ss = new TreeSet();

		ss.add("Kanha");
		ss.add("Rajesh");
		ss.add("Rahul");
		ss.add("Ashish");
		ss.add("Badhe");

		System.out.println("SortedSet : " + ss);

		System.out.println("headSet() => " + ss.headSet("Kanha"));
		System.out.println("tailSet() => " + ss.tailSet("Kanha"));
		System.out.println("subSet() => " + ss.subSet("Ashish","Rahul"));
		System.out.println("first() => " + ss.first());
		System.out.println("last() => " + ss.last());
	}
}

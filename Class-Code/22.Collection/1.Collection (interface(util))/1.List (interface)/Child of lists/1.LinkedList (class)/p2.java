import java.util.*;
class CricPlayer{

	String name = null;
	int jerNo = 0;

	CricPlayer(String name,int jerNo){
	
		this.name = name;
		this.jerNo = jerNo;
	}

	public String toString(){
	
		return jerNo + " : " + name;
	}
}
class LinkedListDemo{

	public static void main(String[]args){
	
		LinkedList ll = new LinkedList();
		ll.add(new CricPlayer("Kohali",18));
		ll.add(new CricPlayer("Rohit",45));
		ll.add(new CricPlayer("Dhoni",7));

		System.out.println(ll);
	}
}

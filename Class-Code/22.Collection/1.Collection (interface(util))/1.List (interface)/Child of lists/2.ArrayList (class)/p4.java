import java.util.*;
class ITCompany{

	String cmpName = null;
	int empCount = 0;

	ITCompany(String cmpName,int empCount){
	
		this.cmpName = cmpName;
		this.empCount = empCount;
	}
}
class ArrayListDemo{

	public static void main(String[]args){
	
		ArrayList al = new ArrayList();
		al.add(10);
		al.add(20);
		al.add("Shashi");
		al.add(new ITCompany("Binecaps",30));

		System.out.println(al);
	}
}

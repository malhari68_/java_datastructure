import java.util.*;
class ArrayDemo{

	public static void main(String[]args){
	
		ArrayList al = new ArrayList();
		al.add(10);
		al.add(20);
		al.add(30);
		al.add(new Integer(40));
		al.add(30);

		for(Object obj : al){
		
			System.out.println(obj);
		}

		System.out.println("________________________________________");

		for(var obj : al){
		
			System.out.println(al);
		}
		
		System.out.println("________________________________________");

		for(int i = 0;i < al.size();i++){
		
			System.out.println(al.get(i));
		}
	}
}

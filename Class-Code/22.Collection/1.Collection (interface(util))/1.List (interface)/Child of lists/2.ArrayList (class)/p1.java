import java.util.*;
class ArrayListDemo extends ArrayList{

	public static void main(String[]args){
	
		ArrayList al = new ArrayList();
		
		// ArrayList methods =>
		
		// 1) add(E)
                al.add(10);
                al.add(20.5f);
                al.add("Santosh");
                al.add(10);
                al.add(20.5f);
		System.out.println(al);
		System.out.println("-----------------------------------------------");

		// 2) add(int,E)   //(int => index)
		al.add(3,"Core2Web");
		System.out.println(al);
		System.out.println("-----------------------------------------------");

		// 3) size()
		System.out.println(al.size());
		System.out.println("-----------------------------------------------");

		// 4) indexOf(object)
		System.out.println(al.indexOf(20.5f));
		System.out.println("-----------------------------------------------");
		
		// 5) lastIndexOf(object)
		System.out.println(al.lastIndexOf(20.5f));
		System.out.println("-----------------------------------------------");

		// 6) contain(object)
		System.out.println(al.contains("Santosh"));
		System.out.println(al.contains(30));
		System.out.println("-----------------------------------------------");

		// 7) get(int)
		System.out.println(al.get(3));
		System.out.println("-----------------------------------------------");

		// 8) set(int,E)
		System.out.println(al.set(3,"Incubator"));
		System.out.println(al);
		System.out.println("-----------------------------------------------");

		ArrayList al2 = new ArrayList();
		al2.add("Salman");
		al2.add("Shahrukh");
		al2.add("Amir");

		// 9) addAll(collection);
		al.addAll(al2);
		System.out.println(al);
		System.out.println("-----------------------------------------------");

		// 10) AddAll(index,collection)
		al.addAll(3,al2);
		System.out.println(al);
		System.out.println("-----------------------------------------------");

		// 11) removeRange(int,int)         //retrun type protected ahe
		ArrayListDemo obj=new ArrayListDemo();
                obj.add(10);
                obj.add(20.5f);
                obj.add("Santosh");
                obj.add(10);
                obj.add(20.5f);
			
		obj.removeRange(1,2); //3 pasun te (5 - 1) parent remove karto
	        System.out.println(obj);
		System.out.println("-----------------------------------------------");

		// 12) remove(int)
		System.out.println(al.remove(4));
		System.out.println(al);
		System.out.println("-----------------------------------------------");

		// 13) object[] toArray()
		Object arr[] = al.toArray();
		for(Object data : arr){
		
			System.out.println(data + " " );
		}

		System.out.println();

		// 14) clear();
		al.clear();
		System.out.println(al);
		System.out.println("-----------------------------------------------");
	}
}

import java.util.*;
import java.util.concurrent.*;
class BlockingQueueDemo{

	public static void main(String[]args)throws InterruptedException{
	
		BlockingQueue bQueue = new LinkedBlockingQueue(); //LinkedBlockingQueue kade parameter naslela pn constructor ahe
		bQueue.put(10);
		bQueue.put(20);
		bQueue.put(30);

		System.out.println("LinkedBlockingQueue : " + bQueue);

		bQueue.offer(40,5,TimeUnit.SECONDS);
		System.out.println("LinkedBlockingQueue : " + bQueue);

		bQueue.take();
		System.out.println("LinkedBlockingQueue : " + bQueue);

		System.out.println("_______________________________________________________________________________");

		ArrayList al = new ArrayList();
		System.out.println("ArrayList : " + al);
		bQueue.drainTo(al);
		System.out.println("ArrayList : " + al);
		System.out.println("LinkedBlockingQueue : " + bQueue);
	}
}

import java.util.*;
import java.util.concurrent.*;
class BlockingQueueDemo{

	public static void main(String[]args)throws InterruptedException{
	
		BlockingQueue bQueue = new ArrayBlockingQueue(3);
		bQueue.put(10);
		bQueue.put(20);
		bQueue.put(30);

		bQueue.offer(40,5,TimeUnit.SECONDS); //dilelya time madhe size bhetli tr insert,nasta kill hoto
		System.out.println("ArrayBlockingQueue : " + bQueue);

		bQueue.take();
		System.out.println("ArrayBlockingQueue : " + bQueue); //first element remove karne

		System.out.println("____________________________________________________________________________________________");

		ArrayList al = new ArrayList();
		System.out.println("ArrayList : " + al);
		bQueue.drainTo(al);       //Queue la dusrya collection madhe add karnyasatti
		System.out.println("ArrayList : " + al);
		System.out.println("ArrayBlockingQueue : " + bQueue);
	}
}
//OP =>
/*ArrayBlockingQueue : [10, 20, 30]
 * ArrayBlockingQueue : [20, 30]
 * ____________________________________________________________________________________________
 * ArrayList : []
 * ArrayList : [20, 30]
 * ArrayBlockingQueue : []*/

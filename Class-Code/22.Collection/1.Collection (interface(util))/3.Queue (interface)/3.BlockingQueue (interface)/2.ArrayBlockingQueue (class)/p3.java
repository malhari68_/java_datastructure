import java.util.concurrent.*;
class BlockingQueueDemo{

	public static void main(String[]args){
	
		BlockingQueue bQueue = new ArrayBlockingQueue(3);
		bQueue.offer(10);
		bQueue.offer(20);
		bQueue.offer(30);
		bQueue.offer(40); //40 Add nahi honar,ArrayBlocking Queue chi size 3 aslyane ani error pn nahi yenar(offer() method Queue aslyane)

		System.out.println("ArrayBlockingQueue : " + bQueue);
	}
}
//OP => 
/*ArrayBlockingQueu : [10,20,30] */

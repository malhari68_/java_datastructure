//they must be getting size of ArrayBlockingQueue

import java.util.concurrent.*;
class BlockingQueueDemo{

	public static void main(String[]args){
	
		BlockingQueue bQueue = new ArrayBlockingQueue(3);
		bQueue.offer(10);
		bQueue.offer(20);
		bQueue.offer(30);
		
		System.out.println("ArrayBlockingQueue : " + bQueue);
	}
}



import java.util.concurrent.*;
class BlockingQueueDemo{

	public static void main(String[]args)throws InterruptedException{
	
		BlockingQueue bQueue = new ArrayBlockingQueue(3);
		bQueue.offer(10);
		bQueue.offer(20);
		bQueue.offer(30);

		System.out.println("ArrayBlockingQueue : " + bQueue);

		bQueue.put(40);                   //method block hote,karan ArrayBlockingQueue chi size 3 ahe 
		System.out.println("ArrayBlockingQueue : " + bQueue); 
	}
}

import java.util.*;
import java.util.concurrent.*;
class Employee{

	String name;

	Employee(String name){
	
		this.name = name;
	}

	public String toString(){
	
		return name;
	}
}
class SortByName implements Comparator{

	public int compare(Object obj1,Object obj2){
	
		return ((Employee)obj1).name.compareTo(((Employee)obj2).name);
	}
}
class BlockingQueueDemo{

	public static void main(String[]args)throws InterruptedException{
	
		BlockingQueue bQueue = new PriorityBlockingQueue(3,new SortByName());
		bQueue.put(new Employee("Kanha"));
		bQueue.put(new Employee("Ashish"));
		bQueue.put(new Employee("Rahul"));
		System.out.println("PriorityBlockingQueue : " + bQueue);
		System.out.println("____________________________________________________________________________");
		bQueue.offer(new Employee("Badhe"),5,TimeUnit.SECONDS); //size pesha pn jast element add karu shakto
		System.out.println("PriorityBlockingQueue : " + bQueue);
	}
}
//OP =>
/*PriorityBlockingQueue : [Ashish, Kanha, Rahul]
 * ____________________________________________________________________________
 * PriorityBlockingQueue : [Ashish, Badhe, Rahul, Kanha]*/

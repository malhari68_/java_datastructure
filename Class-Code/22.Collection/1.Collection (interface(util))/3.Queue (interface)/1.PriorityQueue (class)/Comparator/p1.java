import java.util.*;
class Project{

        String projName;
        int teamSize;
        int duration;

        Project(String projName,int teamSize,int duration){

                this.projName = projName;
                this.teamSize = teamSize;
                this.duration = duration;
        }

        public String toString(){

                return "{" + projName + "," + teamSize + "," + duration + "}";
        }
}
class SortByName implements Comparator{

	public int compare(Object obj1,Object obj2){
	
		return ((Project)obj1).projName.compareTo(((Project)obj2).projName);
	}
}
class SortBySize implements Comparator{

	public int compare(Object obj1,Object obj2){
	
		return (int) ((Project)obj1).teamSize - ((Project)obj2).teamSize;
	}
}
class SortByduration implements Comparator{

	public int compare(Object obj1,Object obj2){
	
		return (int) ((Project)obj1).duration - ((Project)obj2).duration;
	}
}
class PriorityQueueDemo{

        public static void main(String[]args){

                PriorityQueue pq = new PriorityQueue(new SortByName());
                //PriorityQueue pq = new PriorityQueue(new SortBySize());
                //PriorityQueue pq = new PriorityQueue(new SortByduration());
		pq.offer(new Project("AI",4,3));
                pq.offer(new Project("Java",3,2));
                pq.offer(new Project("ML",5,5));
                pq.offer(new Project("BCT",4,3));

                System.out.println("PriorityQueue : " + pq);
        }
}

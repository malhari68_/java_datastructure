import java.util.*;
class Project{

        String projName;
        int teamSize;
        int duration;

        Project(String projName,int teamSize,int duration){

                this.projName = projName;
                this.teamSize = teamSize;
                this.duration = duration;
        }

        public String toString(){

                return "{" + projName + "," + teamSize + "," + duration + "}";
        }
}
class SortByName implements Comparator<Project>{

        public int compare(Project obj1,Project obj2){

                return obj1.projName.compareTo(obj2.projName);
        }
}
class SortBySize implements Comparator<Project>{

        public int compare(Project obj1,Project obj2){

                return (int) obj1.teamSize - obj2.teamSize;
        }
}
class SortByduration implements Comparator<Project>{

        public int compare(Project obj1,Project obj2){

                return (int) obj1.duration - obj2.duration;
        }
}
class PriorityQueueDemo{

        public static void main(String[]args){

                PriorityQueue pq = new PriorityQueue(new SortByName());
                //PriorityQueue pq = new PriorityQueue(new SortBySize());
                //PriorityQueue pq = new PriorityQueue(new SortByduration());
                pq.offer(new Project("AI",4,3));
                pq.offer(new Project("Java",3,2));
                pq.offer(new Project("ML",5,5));
                pq.offer(new Project("BCT",4,3));

                System.out.println("PriorityQueue : " + pq);
        }
}

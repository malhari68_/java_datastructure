import java.util.*;
class Project implements Comparable{

	String projName;
	int teamSize;
	int duration;

	Project(String projName,int teamSize,int duration){
	
		this.projName = projName;
		this.teamSize = teamSize;
		this.duration = duration;
	}

	public String toString(){
	
		return "{" + projName + "," + teamSize + "," + duration + "}";
	}

	public int compareTo(Object obj){
	
		return this.projName.compareTo(((Project)obj).projName);
	}
	
	/*public int compareTo(Object obj){

                return this.teamSize - ((Project)obj).teamSize;
        }*/
}
class PriorityQueueDemo{

	public static void main(String[]args){
	
		PriorityQueue pq = new PriorityQueue();
		pq.offer(new Project("AI",4,3));
		pq.offer(new Project("Java",3,2));
		pq.offer(new Project("ML",5,5));
		pq.offer(new Project("BCT",4,3));

		System.out.println("PriorityQueue : " + pq);
	}
}

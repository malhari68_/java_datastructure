import java.util.*;
class QueueDemo{

        public static void main(String[]args){

                Queue que = new LinkedList();
               /* que.offer(10);
                que.offer(20);
                que.offer(50);
                que.offer(30);
                que.offer(40);*/

                System.out.println(que);

                que.poll();

                que.remove();

                System.out.println(que);

                System.out.println(que.peek());
                System.out.println(que.element());

                System.out.println(que);
        }
}
//OP =>
/*Exception in thread "main" java.util.NoSuchElementException
        at java.base/java.util.LinkedList.removeFirst(LinkedList.java:274)
        at java.base/java.util.LinkedList.remove(LinkedList.java:689)
        at QueueDemo.main(p2.java:17)*/

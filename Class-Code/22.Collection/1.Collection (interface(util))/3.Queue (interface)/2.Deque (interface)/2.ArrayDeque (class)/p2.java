import java.util.*;
class DequeDemo{

        public static void main(String[]args){

                Deque obj = new ArrayDeque();
                obj.offer(10);
                obj.offer(40);
                obj.offer(20);
                obj.offer(30);

                System.out.println("Deque : " + obj);
		System.out.println("__________________________________________________________________________________________________");
		
		System.out.println("//boolean offerFirst(E);");
		obj.offerFirst(5);
		System.out.println("__________________________________________________________________________________________________");
		
		System.out.println("//boolean offerLast(E);");
		obj.offerLast(50);
		System.out.println("__________________________________________________________________________________________________");
		
		System.out.println(obj);
		System.out.println("__________________________________________________________________________________________________");
		
		System.out.println("//E pollFirst();");
		obj.pollFirst();
		System.out.println("__________________________________________________________________________________________________");
		
		System.out.println("//E pollLast();");
		obj.pollLast();
		System.out.println("__________________________________________________________________________________________________");
		
		System.out.println(obj);
		System.out.println("__________________________________________________________________________________________________");
		
		System.out.println("E peekFirst();");
		obj.peekFirst();
		System.out.println("__________________________________________________________________________________________________");
		
		System.out.println("//E peekLast();");
		obj.peekLast();
		System.out.println("__________________________________________________________________________________________________");
		
		System.out.println(obj);
		System.out.println("__________________________________________________________________________________________________");
		
		System.out.println("//Iterator<E> iterator();");
		Iterator itr1 = obj.iterator();
		while(itr1.hasNext()){
		
			System.out.println(itr1.next());
		}
		System.out.println("__________________________________________________________________________________________________");
		
		System.out.println("//Iterator<E> descendingIterator();");
		Iterator itr2 = obj.descendingIterator();
		while(itr2.hasNext()){
		
			System.out.println(itr2.next());
		}
		System.out.println("__________________________________________________________________________________________________");
        }
}
//OP =>
/*Deque : [10, 40, 20, 30]
__________________________________________________________________________________________________
//boolean offerFirst(E);
__________________________________________________________________________________________________
//boolean offerLast(E);
__________________________________________________________________________________________________
[5, 10, 40, 20, 30, 50]
__________________________________________________________________________________________________
//E pollFirst();
__________________________________________________________________________________________________
//E pollLast();
__________________________________________________________________________________________________
[10, 40, 20, 30]
__________________________________________________________________________________________________
E peekFirst();
__________________________________________________________________________________________________
//E peekLast();
__________________________________________________________________________________________________
[10, 40, 20, 30]
__________________________________________________________________________________________________
//Iterator<E> iterator();
10
40
20
30
__________________________________________________________________________________________________
//Iterator<E> descendingIterator();
30
20
40
10
__________________________________________________________________________________________________*/

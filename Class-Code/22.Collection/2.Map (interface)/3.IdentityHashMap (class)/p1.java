//Duplicate key-value store karnyasatti

import java.util.*;
class IdentityHashMapDemo{

	public static void main(String[]args){
	
		IdentityHashMap hm = new IdentityHashMap();
		hm.put(new Integer(10),"Kanha");
		hm.put(new Integer(10),"Rahul");
		hm.put(new Integer(10),"Badhe");

		System.out.println("IdentityHashMap : " + hm);
	}
}
//OP =>
/*IdentityHashMap : {10=Badhe, 10=Rahul, 10=Kanha}*/

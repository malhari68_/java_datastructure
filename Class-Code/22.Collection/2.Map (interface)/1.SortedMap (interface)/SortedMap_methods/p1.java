import java.util.*;
class TreeMapDemo{

	public static void main(String[]args){
	
		SortedMap tm = new TreeMap();
		tm.put("Ind","India");
		tm.put("Pak","Pakistan");
		tm.put("SL","SriLanka");
		tm.put("Ban","Bangladesh");

		System.out.println(tm);
		System.out.println("________________________________________________________________________");

		//subMap(K, K);
		System.out.println("subMap(K, K) => " + tm.subMap("Aus","Pak"));
		System.out.println("________________________________________________________________________");

		//headMap(K);
		System.out.println("headMap(K) => " + tm.headMap("Pak"));
		System.out.println("________________________________________________________________________");
		
		//tailMap(K);
		System.out.println("tailMap(K) => " + tm.tailMap("Pak"));
		System.out.println("________________________________________________________________________");
		
		//firstKey();
		System.out.println("firstKey() => " + tm.firstKey());
		System.out.println("________________________________________________________________________");
		
		//lastKey();
		System.out.println("lastKey() => " + tm.lastKey());
		System.out.println("________________________________________________________________________");
		
		//keySet();
		System.out.println("keySet() => " + tm.keySet());
		System.out.println("________________________________________________________________________");
		
		//values();
		System.out.println("values() => " + tm.values());
		System.out.println("________________________________________________________________________");
		
		//entrySet();
		System.out.println("entrySet() => " + tm.entrySet());
		System.out.println("________________________________________________________________________");
	}
}
//OP =>
/*{Ban=Bangladesh, Ind=India, Pak=Pakistan, SL=SriLanka}
________________________________________________________________________
subMap(K, K) => {Ban=Bangladesh, Ind=India}
________________________________________________________________________
headMap(K) => {Ban=Bangladesh, Ind=India}
________________________________________________________________________
tailMap(K) => {Pak=Pakistan, SL=SriLanka}
________________________________________________________________________
firstKey() => Ban
________________________________________________________________________
lastKey() => SL
________________________________________________________________________
keySet() => [Ban, Ind, Pak, SL]
________________________________________________________________________
values() => [Bangladesh, India, Pakistan, SriLanka]
________________________________________________________________________
entrySet() => [Ban=Bangladesh, Ind=India, Pak=Pakistan, SL=SriLanka]
________________________________________________________________________*/

import java.util.*;
class NavigableMapDemo {
        public static void main(String[] args) {
                NavigableMap nm = new TreeMap();
                nm.put(1,"Harshal");
                nm.put(2,"Dinesh");
                nm.put(3,"Santosh");
                nm.put(4,"Dipak");
                nm.put(5,"Yogesh");
                nm.put(6,"Suraj");
                System.out.println(nm);
                System.out.println(nm.lowerEntry(3));
                System.out.println(nm.lowerKey(3));
                System.out.println(nm.floorEntry(3));
                System.out.println(nm.floorKey(3));
                System.out.println(nm.ceilingEntry(3));
                System.out.println(nm.ceilingKey(3));
                System.out.println(nm.higherEntry(3));
                System.out.println(nm.higherKey(3));
                System.out.println(nm.pollFirstEntry());
                System.out.println(nm.pollLastEntry());
                System.out.println(nm.firstEntry());
                System.out.println(nm.lastEntry());
                System.out.println(nm.navigableKeySet());
                System.out.println(nm.descendingKeySet());
                System.out.println(nm.subMap(1,true,4,true));
                System.out.println(nm.subMap(1,4));
                System.out.println(nm.headMap(3,true));
                System.out.println(nm.headMap(3));
                System.out.println(nm.tailMap(3,true));
                System.out.println(nm.tailMap(3));
        }

}
//OP =>
/*{1=Harshal, 2=Dinesh, 3=Santosh, 4=Dipak, 5=Yogesh, 6=Suraj}
2=Dinesh
2
3=Santosh
3
3=Santosh
3
4=Dipak
4
1=Harshal
6=Suraj
2=Dinesh
5=Yogesh
[2, 3, 4, 5]
[5, 4, 3, 2]
{2=Dinesh, 3=Santosh, 4=Dipak}
{2=Dinesh, 3=Santosh}
{2=Dinesh, 3=Santosh}
{2=Dinesh}
{3=Santosh, 4=Dipak, 5=Yogesh}
{3=Santosh, 4=Dipak, 5=Yogesh}*/

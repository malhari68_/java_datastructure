import java.util.*;
class HashMapDemo{

	public static void main(String[]args){
	
		HashSet hs = new HashSet();
		hs.add("Kanha");
		hs.add("Ashish");
		hs.add("Badhe");
		hs.add("Rahul");
		System.out.println("HashSet : " + hs);
	
		System.out.println("____________________________________________________________________");

		HashMap hm = new HashMap();
		hm.put("Kanha","Infosys");
		hm.put("Ashish","Barclays");
		hm.put("Badhe","CarPro");
		hm.put("Rahul","BMC");
		System.out.println("HashMap : " + hm);
	}
}
//OP =>
/*HashSet : [Rahul, Ashish, Badhe, Kanha]
____________________________________________________________________
HashMap : {Rahul=BMC, Ashish=Barclays, Badhe=CarPro, Kanha=Infosys}*/

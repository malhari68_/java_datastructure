import java.util.*;
class HashMapDemo{

        public static void main(String[]args){

                HashMap hm = new HashMap();
                hm.put("Kanha","BMC");
                hm.put("Ashish","Barclays");
                hm.put("Badhe","CarPro");
                hm.put("Rahul","BMC");
                System.out.println("HashMap : " + hm);
        }
}
//OP =>
/*HashMap : {Rahul=BMC, Ashish=Barclays, Badhe=CarPro, Kanha=BMC}*/

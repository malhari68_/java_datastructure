import java.util.*;
class LinkedHashmapDemo{

	public static void main(String[]args){
	
		LinkedHashMap hm = new LinkedHashMap();
		hm.put("Java",".java");
		hm.put("Python",".py");
		hm.put("Dart",".dart");
		System.out.println("LinkedHashMap : " + hm);
		
		System.out.println("________________________________________________________________________");

		System.out.println("// public V get(java.lang.Object);");
		System.out.println(hm.get("Python")); 
		
		System.out.println("________________________________________________________________________");
		
		System.out.println("// public java.util.Set<K> keySet();");
		System.out.println(hm.keySet());
		
		System.out.println("________________________________________________________________________");
		
		System.out.println("// public java.util.Collection<V> values();");
		System.out.println(hm.values());
		
		System.out.println("________________________________________________________________________");
		
		System.out.println("// public java.util.Set<java.util.Map$Entry<K, V>> entrySet();i");
		System.out.println(hm.entrySet());
	}
}
//OP =>
/*LinkedHashMap : {Java=.java, Python=.py, Dart=.dart}
________________________________________________________________________
// public V get(java.lang.Object);
.py
________________________________________________________________________
// public java.util.Set<K> keySet();
[Java, Python, Dart]
________________________________________________________________________
// public java.util.Collection<V> values();
[.java, .py, .dart]
________________________________________________________________________
// public java.util.Set<java.util.Map$Entry<K, V>> entrySet();i
[Java=.java, Python=.py, Dart=.dart]*/

import java.util.*;
class Movies{

	String movieName = null;
	double totColl = 0.0;
	float imdbRating = 0.0f;

	Movies(String movieName,double totColl,float imdbRating){
	
		this.movieName = movieName;
		this.totColl = totColl;
		this.imdbRating = imdbRating;
	}

	public String toString(){
	
		return "{" + movieName + " , " + totColl + " , " + imdbRating + "}";
	}
}
class SortByName implements Comparator{

	public int compare(Object obj1,Object obj2){
	
		return ((Movies)obj1).movieName.compareTo(((Movies)obj2).movieName);
	}
}
class SortColl implements Comparator{

	public int compare(Object obj1,Object obj2){
	
		return (int) (((Movies)obj1).totColl - ((Movies)obj2).totColl);
	}
}
class SortRating implements Comparator{

	public int compare(Object obj1,Object obj2){
	
		return (int) (((Movies)obj1).imdbRating - ((Movies)obj2).imdbRating);
	}
}
class UserListSort{

	public static void main(String[]args){
	
		ArrayList al = new ArrayList();

		al.add(new Movies("RHTDM",200.00,8.5f));
		al.add(new Movies("Ved",75.00,7.5f));
		al.add(new Movies("Sairat",100.00,8.9f));
		al.add(new Movies("Bajrangi",500.00,9.9f));

		System.out.println("ArrayList : " + al);
		System.out.println("_____________________________________________________________________________________________________________________");

		Collections.sort(al,new SortByName());
		System.out.println("Movie Name : " + al);
		System.out.println("_____________________________________________________________________________________________________________________");
		
		Collections.sort(al,new SortByColl());
		System.out.println("Total Collection : " + al);
		System.out.println("______________________________________________________________________________________________________________________");
		
		Collections.sort(al,new SortByRating());
		System.out.println("IMDB Rating : " + al);
		System.out.println("______________________________________________________________________________________________________________________");
	}
}

import java.util.*;
class Platform{

        String str = null;
        int foundYear = 0;

        Platform(String str,int foundYear){

                this.str = str;
                this.foundYear = foundYear;
        }

        public String toString(){

                return "{" + str + ":" + foundYear + "}";
        }
}
class SortByName implements Comparator<Platform>{

	public int compare(Platform obj1,Platform obj2){
	
		return obj1.str.compareTo(obj2.str);
	}
}
class SortByYear implements Comparator<Platform>{

	public int compare(Platform obj1,Platform obj2){
	
		return (int) (obj1.foundYear - obj2.foundYear);
	}
}
class TreeMapDemo{

        public static void main(String[]args){

                //TreeMap tm = new TreeMap(new SortByName());
                TreeMap tm = new TreeMap(new SortByYear());
                tm.put(new Platform("Youtube",2005),"Google");
                tm.put(new Platform("Instagram",2010),"Meta");
                tm.put(new Platform("Facebook",2004),"Meta");
                tm.put(new Platform("ChatGPT",2023),"OpenAI");

                System.out.println(tm);
        }
}
//OP =>
/*{{Facebook:2004}=Meta, {Youtube:2005}=Google, {Instagram:2010}=Meta, {ChatGPT:2023}=OpenAI}*/

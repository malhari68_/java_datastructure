import java.io.*;
class ArrayDemo{

        public static void main(String[]args)throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size of array : ");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];
                System.out.println("Enter Elements :");
                for(int i = 0;i < arr.length;i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("Print user i/o : ");

                for(int i = 0;i < arr.length;i++){

                        System.out.println(arr[i]);
                }
        }
}

/*Enter size of array :
3
Enter Elements :
1
2
3
Print user i/o :
1
2
3*/
abstract class Defence{

        void Duty(){

                System.out.println("Duty = Security");
        }

        abstract void Uniform();
}
class Army extends Defence{

        void Uniform(){

                System.out.println("Uniform = Black");
        }
}
class Client{

        public static void main(String[]args){

                Defence obj = new Army();
                obj.Duty();
                obj.Uniform();
        }
}

abstract class RBI{

        void Rule(){

                System.out.println("Rule = Investment Limit");
        }

        abstract void RateOfInterest();
}
class SBI extends RBI{

        void RateOfInterest(){

                System.out.println("Rate of Interest = 17%");
        }
}
class Client{

        public static void main(String[]args){

                RBI obj = new SBI();
                obj.Rule();
                obj.RateOfInterest();
        }
}

abstract class SujataMastani{

	void Product(){
	
		System.out.println("Product = Mastani");
	}

	abstract void Price();
}
class Baramati extends SujataMastani{

	void Price(){
	
		System.out.println("Price in Baramati = 110");
	}
}
class Client{

	public static void main(String[]args){
	
		SujataMastani obj = new Baramati();
		obj.Product();
		obj.Price();
	}
}

abstract class Parent{

        void Career(){

                System.out.println("Doctor");
        }

        abstract void Marry();
}
class Child extends Parent{

	void Marry(){
	
		System.out.println("Kriti Shetty");
	}
}
class Client{

	public static void main(String[]args){
	
		Child obj = new Child();
		obj.Career();
		obj.Marry();
	}
}

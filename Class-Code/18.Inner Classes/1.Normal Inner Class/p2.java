class Outer{

	class Inner{
	
		void m1(){
		
			System.out.println("In M1-Inner");
		}
	}

	void m2(){
	
		System.out.println("In M2-Outer");
	}
}
class Client{

	public static void main(String[]args){
	
		Outer obj1 = new Outer();
		obj1.m2();

		Outer.Inner obj2 = obj1.new Inner();
		obj2.m1();

		Outer.Inner obj3 = new Outer().new Inner();
		obj3.m1();
	}
}

//eka class cya aat dusra , jr static class Asen tr

class Outer{

	void m1(){
	
		System.out.println("In m1-Outer");
	}

	static class Inner{
	
		void m1(){
		
			System.out.println("In m1-Inner");
		}
	}
}
class Client{

	public static void main(String[]args){
	
		Outer.Inner obj = new Outer.Inner();        //Outer => reference
	        obj.m1();
	}
}

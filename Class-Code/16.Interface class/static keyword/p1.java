interface Demo{

	static void fun(){
	
		System.out.println("In Fun");
	}
}
class DemoChild implements Demo{


}
class Client{

	public static void main(String[]args){
	
		DemoChild obj = new DemoChild();
		obj.fun();        //error
	}
}
/*p1.java:17: error: cannot find symbol
                obj.fun();
                   ^
  symbol:   method fun()
  location: variable obj of type DemoChild
1 error*/

interface Demo1{

	default void fun(){
	
		System.out.println("In Fun Demo1");
	}
}
interface Demo2{

	default void fun(){
	
		System.out.println("In Fun-Demo2");
	}
}
class DemoChild implements Demo1,Demo2{

	public void fun(){
	
		System.out.println("In Fun-DemoChild");
	}
}
class Client{

	public static void main(String[]args){
	
		DemoChild obj1 = new DemoChild();
		obj1.fun();
		
		Demo1 obj2 = new DemoChild();
		obj2.fun();
		
		Demo2 obj3 = new DemoChild();
		obj3.fun();
	}
}

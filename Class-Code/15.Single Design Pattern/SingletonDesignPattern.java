//Multiple Objects banvun nay manun and dusrya class madun object n banvata call karta yeto


class SingletonDesignPattern{

	static SingletonDesignPattern obj = new SingletonDesignPattern();

	private SingletonDesignPattern(){
	
		System.out.println("Constructor");
	}

	static SingletonDesignPattern getObjects(){
	
		return obj;
	}
}
class Client{

	public static void main(String[]args){
	
		SingletonDesignPattern obj1 = SingletonDesignPattern.getObjects();
		System.out.println(obj1);
		
		SingletonDesignPattern obj2 = SingletonDesignPattern.getObjects();
		System.out.println(obj2);
		
		SingletonDesignPattern obj3 = SingletonDesignPattern.getObjects();
		System.out.println(obj3);
	}
}
